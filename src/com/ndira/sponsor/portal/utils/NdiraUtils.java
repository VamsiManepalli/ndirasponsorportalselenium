package com.ndira.sponsor.portal.utils;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class NdiraUtils
{
	private static String filename = "properties.properties";
	public static String getProperty(String key) 
	{
		String value = null;
		Properties prop = new Properties();
    	InputStream input = null;
    	try 
    	{
    		input = NdiraUtils.class.getClassLoader().getResourceAsStream(filename);
    		if(input==null)
    		{
    	            throw new FileNotFoundException("Properties file not found.");
    		}
    		//load a properties file from class path, inside static method
    		prop.load(input);
    		value = prop.getProperty(key);
    	}
    	catch (IOException ex) 
    	{
    		ex.printStackTrace();
        }
    	finally
    	{
        	if(input!=null)
        	{
        		try 
        		{
        			input.close();
        		} 
        		catch (IOException e)
        		{
				e.printStackTrace();
        		}
        	}
        }
		return value;
	}
}
