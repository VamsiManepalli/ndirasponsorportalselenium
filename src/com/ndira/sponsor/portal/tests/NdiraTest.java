package com.ndira.sponsor.portal.tests;

import java.io.IOException;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.server.browserlaunchers.Sleeper;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;
import com.ndira.sponsor.portal.libraries.AddInvestmentNegative;
import com.ndira.sponsor.portal.libraries.CompanyEdit;
import com.ndira.sponsor.portal.libraries.CompanyOverview;
import com.ndira.sponsor.portal.libraries.ForgetPassword;
import com.ndira.sponsor.portal.libraries.ForgetPasswordNegative;
import com.ndira.sponsor.portal.libraries.InvalidLogin;
import com.ndira.sponsor.portal.libraries.InvalidSponsorSignup;
import com.ndira.sponsor.portal.libraries.Logout;
import com.ndira.sponsor.portal.libraries.ManageRemoveTeamMemberNegative;
import com.ndira.sponsor.portal.libraries.ManageTeamNegative;
import com.ndira.sponsor.portal.libraries.MarketingMaterials;
import com.ndira.sponsor.portal.libraries.NDIRALoginAdobeSignWidget;
import com.ndira.sponsor.portal.libraries.NDIRALoginDidYouKnow;
import com.ndira.sponsor.portal.libraries.NDIRALoginDidYouKnowDelete;
import com.ndira.sponsor.portal.libraries.NDIRALoginMarketingMaterials;
import com.ndira.sponsor.portal.libraries.NDIRALoginRemoveTeammemberNegative;
import com.ndira.sponsor.portal.libraries.NDIRALoginSponsorUserNegative;
import com.ndira.sponsor.portal.libraries.NDIRALoginWebinars;
import com.ndira.sponsor.portal.libraries.NDIRAMarketingMaterialsNegative;
import com.ndira.sponsor.portal.libraries.NDIRAMessageNegative;
import com.ndira.sponsor.portal.libraries.NDIRAUsersignupNegativeStep1;
import com.ndira.sponsor.portal.libraries.NDIRAUsersignupNegativeStep2;
import com.ndira.sponsor.portal.libraries.NDIRAUsersignupNegativeStep3;
import com.ndira.sponsor.portal.libraries.NDiraAddSponsorUsers;
import com.ndira.sponsor.portal.libraries.NDiraDepositories;
import com.ndira.sponsor.portal.libraries.NDiraLogin;
import com.ndira.sponsor.portal.libraries.NDiraloginDepositories;
import com.ndira.sponsor.portal.libraries.NDiraloginFeeSchedules;
import com.ndira.sponsor.portal.libraries.NdiraInvalidLogin;
import com.ndira.sponsor.portal.libraries.NdiraLoginUsers;
import com.ndira.sponsor.portal.libraries.Sponsersignup;
import com.ndira.sponsor.portal.libraries.SponsorAddInvestiment;
import com.ndira.sponsor.portal.libraries.SponsorAddTeamMember;
import com.ndira.sponsor.portal.libraries.SponsorChangePassword;
import com.ndira.sponsor.portal.libraries.SponsorChangePasswordNegative;
import com.ndira.sponsor.portal.libraries.SponsorCompanyProfileUpdate;
import com.ndira.sponsor.portal.libraries.SponsorCurrentInvestments;
import com.ndira.sponsor.portal.libraries.SponsorDepositories;
import com.ndira.sponsor.portal.libraries.SponsorFees;
import com.ndira.sponsor.portal.libraries.SponsorInvestimentStatus;
import com.ndira.sponsor.portal.libraries.SponsorInvestments;
import com.ndira.sponsor.portal.libraries.SponsorManageTeamRemoveTeamMember;
import com.ndira.sponsor.portal.libraries.SponsorManageTeamResetPassword;
import com.ndira.sponsor.portal.libraries.SponsorMessageNegative;
import com.ndira.sponsor.portal.libraries.SponsorMessageToNDira;
import com.ndira.sponsor.portal.libraries.SponsorMyInfoUpdate;
import com.ndira.sponsor.portal.libraries.SponsorRegularLoginLogout;
import com.ndira.sponsor.portal.libraries.SponsorSignupFAQs;
import com.ndira.sponsor.portal.libraries.SponsorSignupMarketingMaterials;
import com.ndira.sponsor.portal.libraries.SponsorSignupWebinarVideos;
import com.ndira.sponsor.portal.libraries.SponsorUserRemove;
import com.ndira.sponsor.portal.libraries.SponsorUserResetpassword;
import com.ndira.sponsor.portal.libraries.SponsorUsersignupNegativeStep1;
import com.ndira.sponsor.portal.libraries.SponsorUsersignupNegativeStep2;
import com.ndira.sponsor.portal.libraries.SponsorUsersignupNegativeStep3;
import com.ndira.sponsor.portal.libraries.Sponsorloginlogout;
import com.ndira.sponsor.portal.libraries.Sponsorlogout;
import com.ndira.sponsor.portal.libraries.Sponsormessages;
import com.ndira.sponsor.portal.libraries.UserSignUpwithSponser;
import com.ndira.sponsor.portal.libraries.UserSignupwithNDiralogin;
import com.ndira.sponsor.portal.utils.NDiraConstants;
import com.ndira.sponsor.portal.utils.NdiraUtils;
import com.ndira.sponsor.portal.utils.XLUtils;

public class NdiraTest extends NDiraConstants
{
	XLUtils xl = new XLUtils();
	/* String xlfile="C:\\Users\\DELL\\Desktop\\Ndira_Testdata2806.xlsx"; */
	String xlfile = NdiraUtils.getProperty("file.xlfile.path");
	String tcsheet = "TestCases";
	String tssheet = "TestSteps";
	int tccount, tscount;
	String tcexe;
	String tcid, tsid, keyword;
	String tcres = "";
	boolean res = false;

	NDiraLogin nd = new NDiraLogin();
	NdiraInvalidLogin nl = new NdiraInvalidLogin();	
	UserSignUpwithSponser usp = new UserSignUpwithSponser();
	Sponsersignup sup = new Sponsersignup();
	CompanyEdit ce = new CompanyEdit();
	UserSignupwithNDiralogin usnl = new UserSignupwithNDiralogin();
	CompanyOverview co = new CompanyOverview();
	Logout out = new Logout();
	Sponsormessages sm = new Sponsormessages();
	SponsorInvestments si = new SponsorInvestments();
	SponsorFees sf = new SponsorFees();
	NDiraAddSponsorUsers su = new NDiraAddSponsorUsers();
	SponsorUserRemove sremove = new SponsorUserRemove();
	SponsorUserResetpassword srp = new SponsorUserResetpassword();
	MarketingMaterials mm = new MarketingMaterials();
	NDiraDepositories ndd = new NDiraDepositories();
	Sponsorloginlogout sl = new Sponsorloginlogout();
	Sponsorlogout slog = new Sponsorlogout();
	SponsorCompanyProfileUpdate spe = new SponsorCompanyProfileUpdate();
	SponsorAddInvestiment sai = new SponsorAddInvestiment();
	SponsorCurrentInvestments scid = new SponsorCurrentInvestments();
	UserSignUpwithSponser uss = new UserSignUpwithSponser();
	SponsorAddTeamMember sat = new SponsorAddTeamMember();
	SponsorManageTeamResetPassword smrp = new SponsorManageTeamResetPassword();
	SponsorManageTeamRemoveTeamMember smrt = new SponsorManageTeamRemoveTeamMember();
	SponsorDepositories sld = new SponsorDepositories();
	SponsorMessageToNDira smsg = new SponsorMessageToNDira();
	SponsorMyInfoUpdate sci = new SponsorMyInfoUpdate();
	NdiraLoginUsers nu = new NdiraLoginUsers();
	SponsorSignupMarketingMaterials smm = new SponsorSignupMarketingMaterials();
	SponsorSignupWebinarVideos swv = new SponsorSignupWebinarVideos();
	SponsorSignupFAQs sFAQs = new SponsorSignupFAQs();
	NDIRALoginDidYouKnow ndk = new NDIRALoginDidYouKnow();
	NDIRALoginDidYouKnowDelete ndkd = new NDIRALoginDidYouKnowDelete();
	NDIRALoginMarketingMaterials nmm = new NDIRALoginMarketingMaterials();
	NDIRALoginWebinars nlw = new NDIRALoginWebinars();
	NDiraloginFeeSchedules nfs = new NDiraloginFeeSchedules();
	NDiraloginDepositories nld = new NDiraloginDepositories();
	ForgetPassword fp = new ForgetPassword();
	InvalidLogin il = new InvalidLogin();
	InvalidSponsorSignup isu = new InvalidSponsorSignup();
	NDIRALoginSponsorUserNegative nsun = new NDIRALoginSponsorUserNegative();
	NDIRALoginRemoveTeammemberNegative nrtn = new NDIRALoginRemoveTeammemberNegative();
	NDIRAMessageNegative nmn = new NDIRAMessageNegative();
	NDIRAMarketingMaterialsNegative nmmn = new NDIRAMarketingMaterialsNegative();
	NDIRAUsersignupNegativeStep1 nusn = new NDIRAUsersignupNegativeStep1();
	NDIRAUsersignupNegativeStep2 nusn2 = new NDIRAUsersignupNegativeStep2();
	NDIRAUsersignupNegativeStep3 nusn3 = new NDIRAUsersignupNegativeStep3();
	SponsorRegularLoginLogout srlo = new SponsorRegularLoginLogout();
	ForgetPasswordNegative fpn = new ForgetPasswordNegative();
	AddInvestmentNegative ain = new AddInvestmentNegative();
	ManageTeamNegative mtn = new ManageTeamNegative();
	ManageRemoveTeamMemberNegative mremn = new ManageRemoveTeamMemberNegative();
	SponsorMessageNegative smn = new SponsorMessageNegative();
	SponsorUsersignupNegativeStep1 sun1 = new SponsorUsersignupNegativeStep1();
	SponsorUsersignupNegativeStep2 sun2 = new SponsorUsersignupNegativeStep2();
	SponsorUsersignupNegativeStep3 sun3 = new SponsorUsersignupNegativeStep3();
	NDIRALoginAdobeSignWidget nla = new NDIRALoginAdobeSignWidget();
	SponsorChangePassword scp=new SponsorChangePassword();
	SponsorChangePasswordNegative scpn=new SponsorChangePasswordNegative();
	SponsorInvestimentStatus sis=new SponsorInvestimentStatus();
	
	
	
	@Test
	public void NdiraTest() throws IOException 
	{
		tccount = xl.getRowCount(xlfile, tcsheet);
		tscount = xl.getRowCount(xlfile, tssheet);
		for (int i = 1; i <= tccount; i++)
		{
			tcexe = xl.getCellData(xlfile, tcsheet, i, 2);
			if (tcexe.equalsIgnoreCase("Y"))
			{
				tcid = xl.getCellData(xlfile, tcsheet, i, 0);
				for (int j = 1; j <= tscount; j++) 
				{
					try
					{
						tsid = xl.getCellData(xlfile, tssheet, j, 0);
					} 
					catch (Exception e) 
					{
						System.out.println("");
					}
					if (tcid.equalsIgnoreCase(tsid)) 
					{
						keyword = xl.getCellData(xlfile, tssheet, j, 4);

						switch (keyword.toUpperCase()) 
						{
						
						
						case "NDIRALOGIN":
							nd.suname = xl.getCellData(xlfile, tssheet, j, 5);
							nd.spwd = xl.getCellData(xlfile, tssheet, j, 6);
							res = nd.Login();
							break;

						case "NDIRAINVALIDLOGIN":
							nl.isuname = xl.getCellData(xlfile, tssheet, j, 5);
							nl.ispwd = xl.getCellData(xlfile, tssheet, j, 6);
							res = nl.ndiraInvalidLogin(nl.isuname, nl.ispwd);
							break;
						
						
						case "USERSIGNUPWITHSPONSER":
							res = usp.ClientSignUp();
							break;

						case "SPONSERSIGNUP":
							sup.company = xl.getCellData(xlfile, tssheet, j, 5);
							sup.Category = xl.getCellData(xlfile, tssheet, j, 6);

							sup.fname = xl.getCellData(xlfile, tssheet, j, 7);
							sup.lname = xl.getCellData(xlfile, tssheet, j, 8);
							sup.email = xl.getCellData(xlfile, tssheet, j, 9);
							sup.number = xl.getCellData(xlfile, tssheet, j, 10);
							sup.password = xl.getCellData(xlfile, tssheet, j, 11);
							sup.cpass = xl.getCellData(xlfile, tssheet, j, 12);
							res = sup.sponserSignup();
							Sleeper.sleepTightInSeconds(2);
							break;

						case "COMPANYEDIT":
							ce.saddress1 = xl.getCellData(xlfile, tssheet, j, 5);
							ce.saddress2 = xl.getCellData(xlfile, tssheet, j, 6);
							ce.scity = xl.getCellData(xlfile, tssheet, j, 7);
							ce.szipcode = xl.getCellData(xlfile, tssheet, j, 9);
							ce.state=xl.getCellData(xlfile, tssheet, j, 8);							
							ce.website=xl.getCellData(xlfile, tssheet, j, 10);							
							ce.p1=xl.getCellData(xlfile, tssheet, j, 11);							
							ce.p2=xl.getCellData(xlfile, tssheet, j, 12);
							res = ce.companyEdit(ce.saddress1, ce.saddress2, ce.scity, ce.szipcode,ce.state,ce.website,ce.p1,ce.p2);
							d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
							out.Logout();
							break;

						case "USERSIGNUPWITHNDIRA":
							usnl.fname = xl.getCellData(xlfile, tssheet, j, 5);

							usnl.lname = xl.getCellData(xlfile, tssheet, j, 6);

							usnl.email = xl.getCellData(xlfile, tssheet, j, 7);

							usnl.pass = xl.getCellData(xlfile, tssheet, j, 8);

							usnl.cpass = xl.getCellData(xlfile, tssheet, j, 9);

							usnl.DOB = xl.getCellData(xlfile, tssheet, j, 10);

							usnl.SSN = xl.getCellData(xlfile, tssheet, j, 11);

							usnl.maritalstatus = xl.getCellData(xlfile, tssheet, j, 12);

							usnl.phone = xl.getCellData(xlfile, tssheet, j, 13);

							usnl.address1 = xl.getCellData(xlfile, tssheet, j, 14);

							usnl.address2 = xl.getCellData(xlfile, tssheet, j, 15);

							usnl.city = xl.getCellData(xlfile, tssheet, j, 16);

							usnl.state = xl.getCellData(xlfile, tssheet, j, 17);

							usnl.zipcode = xl.getCellData(xlfile, tssheet, j, 18);

							usnl.typeofaccount = xl.getCellData(xlfile, tssheet, j, 19);

							usnl.investiment = xl.getCellData(xlfile, tssheet, j, 20);

							usnl.phonen = xl.getCellData(xlfile, tssheet, j, 21);

							usnl.cardno = xl.getCellData(xlfile, tssheet, j, 22);

							usnl.cardExpiryDate = xl.getCellData(xlfile, tssheet, j, 23);

							usnl.cardName = xl.getCellData(xlfile, tssheet, j, 24);

							usnl.cardCVCNumber = xl.getCellData(xlfile, tssheet, j, 25);

							res = usnl.UserSignupwithNDiralogin();
							xl.setCellData(xlfile, tssheet, j, 26, UserSignupwithNDiralogin.AccNo);
							break;

						case "COMPANYOVERVIEW":
							co.ctype=xl.getCellData(xlfile, tssheet, j, 5);
							co.ucrelatipnship = xl.getCellData(xlfile, tssheet, j, 6);
							co.uspicategory = xl.getCellData(xlfile, tssheet, j, 7);
							co.uusrelatiionship = xl.getCellData(xlfile, tssheet, j, 8);
							co.ustype = xl.getCellData(xlfile, tssheet, j, 9);
							co.sfunctions=xl.getCellData(xlfile, tssheet, j, 10);
							co.uexpiry = xl.getCellData(xlfile, tssheet, j, 11);
							co.amin=xl.getCellData(xlfile, tssheet, j, 12);
							co.amax=xl.getCellData(xlfile, tssheet, j, 13);
							res = co.companyOverview();
							Sleeper.sleepTightInSeconds(2);
							out.Logout();
							break;

						case "SPONSORMESSAGE":
							sm.message = xl.getCellData(xlfile, tssheet, j, 5);
							res = sm.sponsermessages();
							out.Logout();
							break;

						case "INVESTMENTS":
							res = si.SponsorInvestiments();
							out.Logout();
							break;

						case "FEES":
							res = sf.SponsorFees();
							out.Logout();
							break;

						case "NDIRAADDTEAMMEMBER":
							su.fname = xl.getCellData(xlfile, tssheet, j, 5);
							su.lname = xl.getCellData(xlfile, tssheet, j, 6);
							su.email = xl.getCellData(xlfile, tssheet, j, 7);
							su.role=xl.getCellData(xlfile, tssheet, j, 8);
							res = su.NDiraAddSponsorUser();
							out.Logout();
							break;

						case "SPONSORUSERSREMOVE":
							res = sremove.Sponsoruserremove();
							out.Logout();
							break;

						case "SPONSORUSERSRESETPASSWORD":
							res = srp.SponsorUserResetpassword();
							out.Logout();
							break;

						case "MARKETINGMATERIALS":
							mm.Title = xl.getCellData(xlfile, tssheet, j, 5);
							mm.Desc = xl.getCellData(xlfile, tssheet, j, 6);
							res = mm.MarketingMaterials();
							out.Logout();
							break;

						case "ADOBESIGNWIDGET":
							res = nla.NDIRALoginAdobeSignWidget();
							out.Logout();
							break;

						case "NDIRADEPOSITORIES":
							res = ndd.Ndiradepositories();
							out.Logout();
							break;

						case "SPONSORLOGINLOGOUT":
							sl.email = xl.getCellData(xlfile, tssheet, 1, 9);
							sl.password = xl.getCellData(xlfile, tssheet, 1, 11);
							res = sl.Login(sl.email, sl.password);
							break;

						case "SPONSORCOMPANYPROFILE":
							spe.Address1 = xl.getCellData(xlfile, tssheet, j, 5);
							spe.Address2 = xl.getCellData(xlfile, tssheet, j, 6);
							spe.city = xl.getCellData(xlfile, tssheet, j, 7);
							spe.state1 = xl.getCellData(xlfile, tssheet, j, 8);
							spe.Zipcode = xl.getCellData(xlfile, tssheet, j, 9);
							spe.website = xl.getCellData(xlfile, tssheet, j, 10);
							spe.Phone1 = xl.getCellData(xlfile, tssheet, j, 11);
							spe.Phone2 = xl.getCellData(xlfile, tssheet, j, 12);
							res = spe.SponsorCompanyProfileUpdate();
							slog.Logout();

							break;

						case "SPONSORADDINVESTMENT":
							sai.name = xl.getCellData(xlfile, tssheet, j, 5);
							sai.Investment = xl.getCellData(xlfile, tssheet, j, 6);
							sai.text = xl.getCellData(xlfile, tssheet, j, 7);
							res = sai.SponsorAddInvestment();
							slog.Logout();

							break;

						case "CURRENTINVESTMENTDELETE":
							res = scid.SponsorCurrentInvestigations();
							slog.Logout();
							break;

						case "USERSIGNUPWITHSPONSOR":
							uss.fname = xl.getCellData(xlfile, tssheet, j, 5);

							uss.lname = xl.getCellData(xlfile, tssheet, j, 6);

							uss.email = xl.getCellData(xlfile, tssheet, j, 7);

							uss.pass = xl.getCellData(xlfile, tssheet, j, 8);

							uss.cpass = xl.getCellData(xlfile, tssheet, j, 9);

							uss.DOB = xl.getCellData(xlfile, tssheet, j, 10);

							uss.SSN = xl.getCellData(xlfile, tssheet, j, 11);

							uss.maritalstatus = xl.getCellData(xlfile, tssheet, j, 12);

							uss.phone = xl.getCellData(xlfile, tssheet, j, 13);

							uss.address1 = xl.getCellData(xlfile, tssheet, j, 14);

							uss.address2 = xl.getCellData(xlfile, tssheet, j, 15);

							uss.city = xl.getCellData(xlfile, tssheet, j, 16);

							uss.state = xl.getCellData(xlfile, tssheet, j, 17);

							uss.zipcode = xl.getCellData(xlfile, tssheet, j, 18);

							uss.typeofaccount = xl.getCellData(xlfile, tssheet, j, 19);

							uss.investiment = xl.getCellData(xlfile, tssheet, j, 20);

							uss.phonen = xl.getCellData(xlfile, tssheet, j, 21);

							uss.cardno = xl.getCellData(xlfile, tssheet, j, 22);

							uss.cardExpiryDate = xl.getCellData(xlfile, tssheet, j, 23);

							uss.cardName = xl.getCellData(xlfile, tssheet, j, 24);

							uss.cardCVCNumber = xl.getCellData(xlfile, tssheet, j, 25);
							res = uss.ClientSignUp();
							
							d.get(url);
							break;

						case "SPONSORADDTEAMMEMBER":
							sat.fname = xl.getCellData(xlfile, tssheet, j, 5);
							sat.lname = xl.getCellData(xlfile, tssheet, j, 6);
							sat.email = xl.getCellData(xlfile, tssheet, j, 7);
							sat.role = xl.getCellData(xlfile, tssheet, j, 8);
							res = sat.SponsorAddTeamMember();
							slog.Logout();
							break;

						case "SPONSORRESETPASSWORD":
							res = smrp.SponsorManageTeamResetPassword();
							slog.Logout();
							break;

						case "SPONSORREMOVETEAMMEMBER":
							res = smrt.RemoveTeamMember();
							slog.Logout();
							break;
							
						case "SPONSORDEPOSITORIES":
							res = sld.SponsorDepositories();
							slog.Logout();
							break;

						case "SPONSORMESSAGETONDIRA":
							smsg.text = xl.getCellData(xlfile, tssheet, j, 5);
							res = smsg.SponsorMessageToNDira();
							slog.Logout();
							break;

						case "MYINFOUPDATE":
							sci.fname = xl.getCellData(xlfile, tssheet, j, 5);
							sci.lname = xl.getCellData(xlfile, tssheet, j, 6);
							sci.email = xl.getCellData(xlfile, tssheet, j, 7);							
							res = sci.SponsorMyInfoUpdate();
							xl.setCellData(xlfile, tssheet, 1, 9, sci.email);
							slog.Logout();
							break;

						case "NDIRAUSERS":
							res = nu.NdiraLoginUsers();
							out.Logout();
							break;

						case "SPONSORMARKETINGMATERIALS":
							res = smm.SponsorSignupMarketingMaterials();
							slog.Logout();
							break;

						case "SPONSORWEBINARVIDEO":
							res = swv.SponsorSignupWebinarVideo();
							slog.Logout();
							break;

						case "SPONSORFAQS":
							res = sFAQs.SponsorSignupFAQs();
							slog.Logout();
							break;

						case "NDIRADIDYOUKNOW":
							ndk.subject = xl.getCellData(xlfile, tssheet, j, 5);
							res = ndk.NDiraDidYouKnow();
							out.Logout();
							break;

						case "NDIRADIDYOUKNOWDELETE":
							res = ndkd.NDIRALoginDidYouKnowDelete();
							out.Logout();
							break;

						case "NDIRAMARKETINGMATERIALS":
							res = nmm.NDIRALoginMarketingMaterials();
							out.Logout();
							break;

						case "NDIRAWEBINARS":
							res = nlw.NDIRALoginWebinars();
							out.Logout();
							break;

						case "NDIRASITEDATAFEESCHEDULES":
							res = nfs.NDiraloginFeeSchedules();
							out.Logout();
							break;

						case "NDIRASITEDATADEPOSITORIES":
							res = nld.NDiraloginDepositories();
							out.Logout();
							break;

						case "FORGETPASSWORD":
							fp.email = xl.getCellData(xlfile, tssheet, j, 5);
							res = fp.Forgetpassword();
							break;

						case "INVALIDLOGIN":
							il.uname = xl.getCellData(xlfile, tssheet, j, 5);
							il.pass = xl.getCellData(xlfile, tssheet, j, 6);
							res = il.InvalidLogin();
							d.get(url);
							break;

						case "INVALIDLOGIN1":
							il.uname = xl.getCellData(xlfile, tssheet, j, 5);
							il.pass = xl.getCellData(xlfile, tssheet, j, 6);
							res = il.InvalidLogin();
							d.get(url);
							break;

						case "INVALIDLOGIN2":
							il.uname = xl.getCellData(xlfile, tssheet, j, 5);
							il.pass = xl.getCellData(xlfile, tssheet, j, 6);
							res = il.InvalidLogin();
							d.get(url);
							break;

						case "INVALIDLOGIN3":
							il.uname = xl.getCellData(xlfile, tssheet, j, 5);
							il.pass = xl.getCellData(xlfile, tssheet, j, 6);
							res = il.InvalidLogin();
							d.get(url);
							break;

						case "INVALIDLOGIN4":
							il.uname = xl.getCellData(xlfile, tssheet, j, 5);
							il.pass = xl.getCellData(xlfile, tssheet, j, 6);
							res = il.InvalidLogin();
							d.get(url);
							break;

						case "INVALIDLOGIN5":
							il.uname = xl.getCellData(xlfile, tssheet, j, 5);
							il.pass = xl.getCellData(xlfile, tssheet, j, 6);
							res = il.InvalidLogin();
							d.get(url);
							break;

						case "INVALIDSPONSORSIGNUP":
							isu.company = xl.getCellData(xlfile, tssheet, j, 5);
							isu.Category = xl.getCellData(xlfile, tssheet, j, 6);
							isu.fname = xl.getCellData(xlfile, tssheet, j, 7);
							isu.lname = xl.getCellData(xlfile, tssheet, j, 8);
							isu.email = xl.getCellData(xlfile, tssheet, j, 9);
							isu.number = xl.getCellData(xlfile, tssheet, j, 10);
							isu.password = xl.getCellData(xlfile, tssheet, j, 11);
							isu.cpass = xl.getCellData(xlfile, tssheet, j, 12);
							res = isu.InvalidSponsorSignup();
							break;

						case "INVALIDSPONSORSIGNUP1":
							isu.company = xl.getCellData(xlfile, tssheet, j, 5);
							isu.Category = xl.getCellData(xlfile, tssheet, j, 6);

							isu.fname = xl.getCellData(xlfile, tssheet, j, 7);
							isu.lname = xl.getCellData(xlfile, tssheet, j, 8);
							isu.email = xl.getCellData(xlfile, tssheet, j, 9);
							isu.number = xl.getCellData(xlfile, tssheet, j, 10);
							isu.password = xl.getCellData(xlfile, tssheet, j, 11);
							isu.cpass = xl.getCellData(xlfile, tssheet, j, 12);

							res = isu.InvalidSponsorSignup();
							break;

						case "INVALIDSPONSORSIGNUP2":

							isu.company = xl.getCellData(xlfile, tssheet, j, 5);
							isu.Category = xl.getCellData(xlfile, tssheet, j, 6);

							isu.fname = xl.getCellData(xlfile, tssheet, j, 7);
							isu.lname = xl.getCellData(xlfile, tssheet, j, 8);
							isu.email = xl.getCellData(xlfile, tssheet, j, 9);
							isu.number = xl.getCellData(xlfile, tssheet, j, 10);
							isu.password = xl.getCellData(xlfile, tssheet, j, 11);
							isu.cpass = xl.getCellData(xlfile, tssheet, j, 12);

							res = isu.InvalidSponsorSignup();
							break;

						case "INVALIDSPONSORSIGNUP3":

							isu.company = xl.getCellData(xlfile, tssheet, j, 5);
							isu.Category = xl.getCellData(xlfile, tssheet, j, 6);

							isu.fname = xl.getCellData(xlfile, tssheet, j, 7);
							isu.lname = xl.getCellData(xlfile, tssheet, j, 8);
							isu.email = xl.getCellData(xlfile, tssheet, j, 9);
							isu.number = xl.getCellData(xlfile, tssheet, j, 10);
							isu.password = xl.getCellData(xlfile, tssheet, j, 11);
							isu.cpass = xl.getCellData(xlfile, tssheet, j, 12);

							res = isu.InvalidSponsorSignup();
							break;

						case "INVALIDSPONSORSIGNUP4":

							isu.company = xl.getCellData(xlfile, tssheet, j, 5);
							isu.Category = xl.getCellData(xlfile, tssheet, j, 6);

							isu.fname = xl.getCellData(xlfile, tssheet, j, 7);
							isu.lname = xl.getCellData(xlfile, tssheet, j, 8);
							isu.email = xl.getCellData(xlfile, tssheet, j, 9);
							isu.number = xl.getCellData(xlfile, tssheet, j, 10);
							isu.password = xl.getCellData(xlfile, tssheet, j, 11);
							isu.cpass = xl.getCellData(xlfile, tssheet, j, 12);

							res = isu.InvalidSponsorSignup();
							break;

						case "INVALIDSPONSORSIGNUP5":

							isu.company = xl.getCellData(xlfile, tssheet, j, 5);
							isu.Category = xl.getCellData(xlfile, tssheet, j, 6);

							isu.fname = xl.getCellData(xlfile, tssheet, j, 7);
							isu.lname = xl.getCellData(xlfile, tssheet, j, 8);
							isu.email = xl.getCellData(xlfile, tssheet, j, 9);
							isu.number = xl.getCellData(xlfile, tssheet, j, 10);
							isu.password = xl.getCellData(xlfile, tssheet, j, 11);
							isu.cpass = xl.getCellData(xlfile, tssheet, j, 12);

							res = isu.InvalidSponsorSignup();
							break;

						case "INVALIDSPONSORSIGNUP6":

							isu.company = xl.getCellData(xlfile, tssheet, j, 5);
							isu.Category = xl.getCellData(xlfile, tssheet, j, 6);

							isu.fname = xl.getCellData(xlfile, tssheet, j, 7);
							isu.lname = xl.getCellData(xlfile, tssheet, j, 8);
							isu.email = xl.getCellData(xlfile, tssheet, j, 9);
							isu.number = xl.getCellData(xlfile, tssheet, j, 10);
							isu.password = xl.getCellData(xlfile, tssheet, j, 11);
							isu.cpass = xl.getCellData(xlfile, tssheet, j, 12);

							res = isu.InvalidSponsorSignup();
							break;

						case "INVALIDSPONSORSIGNUP7":

							isu.company = xl.getCellData(xlfile, tssheet, j, 5);
							isu.Category = xl.getCellData(xlfile, tssheet, j, 6);

							isu.fname = xl.getCellData(xlfile, tssheet, j, 7);
							isu.lname = xl.getCellData(xlfile, tssheet, j, 8);
							isu.email = xl.getCellData(xlfile, tssheet, j, 9);
							isu.number = xl.getCellData(xlfile, tssheet, j, 10);
							isu.password = xl.getCellData(xlfile, tssheet, j, 11);
							isu.cpass = xl.getCellData(xlfile, tssheet, j, 12);

							res = isu.InvalidSponsorSignup();
							break;

						case "INVALIDSPONSORSIGNUP8":

							isu.company = xl.getCellData(xlfile, tssheet, j, 5);
							isu.Category = xl.getCellData(xlfile, tssheet, j, 6);

							isu.fname = xl.getCellData(xlfile, tssheet, j, 7);
							isu.lname = xl.getCellData(xlfile, tssheet, j, 8);
							isu.email = xl.getCellData(xlfile, tssheet, j, 9);
							isu.number = xl.getCellData(xlfile, tssheet, j, 10);
							isu.password = xl.getCellData(xlfile, tssheet, j, 11);
							isu.cpass = xl.getCellData(xlfile, tssheet, j, 12);

							res = isu.InvalidSponsorSignup();
							break;

						case "INVALIDSPONSORSIGNUP9":

							isu.company = xl.getCellData(xlfile, tssheet, j, 5);
							isu.Category = xl.getCellData(xlfile, tssheet, j, 6);

							isu.fname = xl.getCellData(xlfile, tssheet, j, 7);
							isu.lname = xl.getCellData(xlfile, tssheet, j, 8);
							isu.email = xl.getCellData(xlfile, tssheet, j, 9);
							isu.number = xl.getCellData(xlfile, tssheet, j, 10);
							isu.password = xl.getCellData(xlfile, tssheet, j, 11);
							isu.cpass = xl.getCellData(xlfile, tssheet, j, 12);

							res = isu.InvalidSponsorSignup();
							break;

						case "INVALIDSPONSORSIGNUP10":

							isu.company = xl.getCellData(xlfile, tssheet, j, 5);
							isu.Category = xl.getCellData(xlfile, tssheet, j, 6);

							isu.fname = xl.getCellData(xlfile, tssheet, j, 7);
							isu.lname = xl.getCellData(xlfile, tssheet, j, 8);
							isu.email = xl.getCellData(xlfile, tssheet, j, 9);
							isu.number = xl.getCellData(xlfile, tssheet, j, 10);
							isu.password = xl.getCellData(xlfile, tssheet, j, 11);
							isu.cpass = xl.getCellData(xlfile, tssheet, j, 12);

							res = isu.InvalidSponsorSignup();
							break;

						case "NDIRASPONSORUSERNEGATIVE":
							nsun.uname = xl.getCellData(xlfile, tssheet, j, 5);
							nsun.pass = xl.getCellData(xlfile, tssheet, j, 6);
							nsun.email = xl.getCellData(xlfile, tssheet, j, 7);
							res = nsun.NDIRALoginSponsorUserNegative();
							Sleeper.sleepTightInSeconds(2);
							out.Logout();
							break;

						case "NDIRASPONSORUSERNEGATIVE1":
							nsun.uname = xl.getCellData(xlfile, tssheet, j, 5);
							nsun.pass = xl.getCellData(xlfile, tssheet, j, 6);
							nsun.email = xl.getCellData(xlfile, tssheet, j, 7);
							res = nsun.NDIRALoginSponsorUserNegative();
							out.Logout();
							break;

						case "NDIRASPONSORUSERNEGATIVE2":
							nsun.uname = xl.getCellData(xlfile, tssheet, j, 5);
							nsun.pass = xl.getCellData(xlfile, tssheet, j, 6);
							nsun.email = xl.getCellData(xlfile, tssheet, j, 7);
							res = nsun.NDIRALoginSponsorUserNegative();
							out.Logout();
							break;

						case "NDIRASPONSORUSERNEGATIVE3":
							nsun.uname = xl.getCellData(xlfile, tssheet, j, 5);
							nsun.pass = xl.getCellData(xlfile, tssheet, j, 6);
							nsun.email = xl.getCellData(xlfile, tssheet, j, 7);
							res = nsun.NDIRALoginSponsorUserNegative();
							out.Logout();
							break;

						case "NDIRASPONSORUSERNEGATIVE4":
							nsun.uname = xl.getCellData(xlfile, tssheet, j, 5);
							nsun.pass = xl.getCellData(xlfile, tssheet, j, 6);
							nsun.email = xl.getCellData(xlfile, tssheet, j, 7);
							res = nsun.NDIRALoginSponsorUserNegative();
							out.Logout();
							break;

						case "NDIRAREMOVETEAMMEMBERNEGATIVE":
							nrtn.uname = xl.getCellData(xlfile, tssheet, j, 5);
							nrtn.pass = xl.getCellData(xlfile, tssheet, j, 6);
							nrtn.email = xl.getCellData(xlfile, tssheet, j, 7);
							nrtn.cemail = xl.getCellData(xlfile, tssheet, j, 8);
							res = nrtn.NDIRALoginRemoveTeammemberNegative();
							out.Logout();
							break;

						case "NDIRAMESSAGENEGATIVE":
							res = nmn.NDIRAMessageNegative();
							out.Logout();
							break;

						case "NDIRAMARKETINGMATERIALSNEGATIVE":
							res = nmmn.NDIRAMarketingMaterialsNegative();
							out.Logout();
							break;

						case "NDIRAUSERSIGNUPNEGATIVE":
							nusn.fname = xl.getCellData(xlfile, tssheet, j, 5);

							nusn.lname = xl.getCellData(xlfile, tssheet, j, 6);

							nusn.email = xl.getCellData(xlfile, tssheet, j, 7);

							nusn.pass = xl.getCellData(xlfile, tssheet, j, 8);

							nusn.cpass = xl.getCellData(xlfile, tssheet, j, 9);

							res = nusn.NDIRAUsersignupNegative();
							String rese = d.findElement(By.xpath("html/body/div[3]/form/div[1]/div[2]/div[2]/div/div[1]/label")).getText();
							System.out.println(rese);
							d.get(url);
							Sleeper.sleepTightInSeconds(5);
							d.findElement(By.xpath("html/body/div[1]/div/div[1]/div[1]/ul/li[6]/a/div[2]")).click();
							break;

						case "NDIRAUSERSIGNUPNEGATIVE1":
							nusn.fname = xl.getCellData(xlfile, tssheet, j, 5);

							nusn.lname = xl.getCellData(xlfile, tssheet, j, 6);

							nusn.email = xl.getCellData(xlfile, tssheet, j, 7);

							nusn.pass = xl.getCellData(xlfile, tssheet, j, 8);

							nusn.cpass = xl.getCellData(xlfile, tssheet, j, 9);

							res = nusn.NDIRAUsersignupNegative();

							String rese1 = d.findElement(By.xpath("html/body/div[3]/form/div[1]/div[2]/div[2]/div/div[3]/label")).getText();
							System.out.println(rese1);

							d.get(url);
							Sleeper.sleepTightInSeconds(5);
							d.findElement(By.xpath("html/body/div[1]/div/div[1]/div[1]/ul/li[6]/a/div[2]")).click();

							break;

						case "NDIRAUSERSIGNUPNEGATIVE2":
							nusn.fname = xl.getCellData(xlfile, tssheet, j, 5);

							nusn.lname = xl.getCellData(xlfile, tssheet, j, 6);

							nusn.email = xl.getCellData(xlfile, tssheet, j, 7);

							nusn.pass = xl.getCellData(xlfile, tssheet, j, 8);

							nusn.cpass = xl.getCellData(xlfile, tssheet, j, 9);

							res = nusn.NDIRAUsersignupNegative();
							String rese2 = d
									.findElement(By.xpath("html/body/div[3]/form/div[2]/div[2]/div[2]/label[1]"))
									.getText();
							System.out.println(rese2);
							d.get(url);
							Sleeper.sleepTightInSeconds(5);
							d.findElement(By.xpath("html/body/div[1]/div/div[1]/div[1]/ul/li[6]/a/div[2]")).click();

							break;

						case "NDIRAUSERSIGNUPNEGATIVE3":
							nusn.fname = xl.getCellData(xlfile, tssheet, j, 5);

							nusn.lname = xl.getCellData(xlfile, tssheet, j, 6);

							nusn.email = xl.getCellData(xlfile, tssheet, j, 7);

							nusn.pass = xl.getCellData(xlfile, tssheet, j, 8);

							nusn.cpass = xl.getCellData(xlfile, tssheet, j, 9);

							res = nusn.NDIRAUsersignupNegative();
							String rese3 = d
									.findElement(By.xpath("html/body/div[3]/form/div[2]/div[2]/div[2]/label[1]"))
									.getText();
							System.out.println(rese3);
							d.get(url);
							Sleeper.sleepTightInSeconds(5);
							d.findElement(By.xpath("html/body/div[1]/div/div[1]/div[1]/ul/li[6]/a/div[2]")).click();

							break;

						case "NDIRAUSERSIGNUPNEGATIVE4":
							nusn.fname = xl.getCellData(xlfile, tssheet, j, 5);

							nusn.lname = xl.getCellData(xlfile, tssheet, j, 6);

							nusn.email = xl.getCellData(xlfile, tssheet, j, 7);

							nusn.pass = xl.getCellData(xlfile, tssheet, j, 8);

							nusn.cpass = xl.getCellData(xlfile, tssheet, j, 9);

							res = nusn.NDIRAUsersignupNegative();
							String rese4 = d
									.findElement(By.xpath("html/body/div[3]/form/div[2]/div[2]/div[2]/label[2]"))
									.getText();
							System.out.println(rese4);
							d.get(url);
							Sleeper.sleepTightInSeconds(5);
							d.findElement(By.xpath("html/body/div[1]/div/div[1]/div[1]/ul/li[6]/a/div[2]")).click();

							break;

						case "NDIRAUSERSIGNUPNEGATIVE5":
							nusn.fname = xl.getCellData(xlfile, tssheet, j, 5);

							nusn.lname = xl.getCellData(xlfile, tssheet, j, 6);

							nusn.email = xl.getCellData(xlfile, tssheet, j, 7);

							nusn.pass = xl.getCellData(xlfile, tssheet, j, 8);

							nusn.cpass = xl.getCellData(xlfile, tssheet, j, 9);

							res = nusn.NDIRAUsersignupNegative();
							String rese5 = d
									.findElement(By.xpath("html/body/div[3]/form/div[2]/div[2]/div[2]/label[2]"))
									.getText();
							System.out.println(rese5);
							d.get(url);
							Sleeper.sleepTightInSeconds(5);
							d.findElement(By.xpath("html/body/div[1]/div/div[1]/div[1]/ul/li[6]/a/div[2]")).click();

							break;

						case "NDIRAUSERSIGNUPNEGATIVE6":
							nusn.fname = xl.getCellData(xlfile, tssheet, j, 5);

							nusn.lname = xl.getCellData(xlfile, tssheet, j, 6);

							nusn.email = xl.getCellData(xlfile, tssheet, j, 7);

							nusn.pass = xl.getCellData(xlfile, tssheet, j, 8);

							nusn.cpass = xl.getCellData(xlfile, tssheet, j, 9);

							res = nusn.NDIRAUsersignupNegative();
							Sleeper.sleepTightInSeconds(5);
							String rese6 = d.findElement(By.xpath("html/body/div[3]/form/div[2]/div[2]/div[2]/label"))
									.getText();
							System.out.println(rese6);
							d.get(url);
							Sleeper.sleepTightInSeconds(5);
							d.findElement(By.xpath("html/body/div[1]/div/div[1]/div[1]/ul/li[6]/a/div[2]")).click();

							break;

						case "NDIRAUSERSIGNUPNEGATIVE7":
							nusn.fname = xl.getCellData(xlfile, tssheet, j, 5);

							nusn.lname = xl.getCellData(xlfile, tssheet, j, 6);

							nusn.email = xl.getCellData(xlfile, tssheet, j, 7);

							nusn.pass = xl.getCellData(xlfile, tssheet, j, 8);

							nusn.cpass = xl.getCellData(xlfile, tssheet, j, 9);

							res = nusn.NDIRAUsersignupNegative();

							Sleeper.sleepTightInSeconds(8);
							String rese7 = d.findElement(By.xpath("html/body/div[3]/form/div[2]/div[2]/div[2]/label"))
									.getText();
							System.out.println(rese7);
							d.get(url);
							Sleeper.sleepTightInSeconds(5);
							d.findElement(By.xpath("html/body/div[1]/div/div[1]/div[1]/ul/li[6]/a/div[2]")).click();

							break;

						case "NDIRAUSERSIGNUPNEGATIVE8":
							nusn.fname = xl.getCellData(xlfile, tssheet, j, 5);

							nusn.lname = xl.getCellData(xlfile, tssheet, j, 6);

							nusn.email = xl.getCellData(xlfile, tssheet, j, 7);

							nusn.pass = xl.getCellData(xlfile, tssheet, j, 8);

							nusn.cpass = xl.getCellData(xlfile, tssheet, j, 9);

							nusn2.DOB = xl.getCellData(xlfile, tssheet, j, 10);

							nusn2.SSN = xl.getCellData(xlfile, tssheet, j, 11);

							nusn2.maritalstatus = xl.getCellData(xlfile, tssheet, j, 12);

							nusn2.phone = xl.getCellData(xlfile, tssheet, j, 13);

							nusn2.address1 = xl.getCellData(xlfile, tssheet, j, 14);

							nusn2.address2 = xl.getCellData(xlfile, tssheet, j, 15);

							nusn2.city = xl.getCellData(xlfile, tssheet, j, 16);

							nusn2.state = xl.getCellData(xlfile, tssheet, j, 17);

							nusn2.zipcode = xl.getCellData(xlfile, tssheet, j, 18);

							nusn2.typeofaccount = xl.getCellData(xlfile, tssheet, j, 19);

							res = nusn2.NDIRAUsersignupNegativeStep2();
							String rese8 = d
									.findElement(
											By.xpath("html/body/div[3]/form/div[1]/div[2]/div[2]/div[1]/div[1]/label"))
									.getText();
							System.out.println(rese8);
							d.get(url);
							Sleeper.sleepTightInSeconds(5);
							d.findElement(By.xpath("html/body/div[1]/div/div[1]/div[1]/ul/li[6]/a/div[2]")).click();

							break;

						case "NDIRAUSERSIGNUPNEGATIVE9":
							nusn.fname = xl.getCellData(xlfile, tssheet, j, 5);

							nusn.lname = xl.getCellData(xlfile, tssheet, j, 6);

							nusn.email = xl.getCellData(xlfile, tssheet, j, 7);

							nusn.pass = xl.getCellData(xlfile, tssheet, j, 8);

							nusn.cpass = xl.getCellData(xlfile, tssheet, j, 9);

							nusn2.DOB = xl.getCellData(xlfile, tssheet, j, 10);

							nusn2.SSN = xl.getCellData(xlfile, tssheet, j, 11);

							nusn2.maritalstatus = xl.getCellData(xlfile, tssheet, j, 12);

							nusn2.phone = xl.getCellData(xlfile, tssheet, j, 13);

							nusn2.address1 = xl.getCellData(xlfile, tssheet, j, 14);

							nusn2.address2 = xl.getCellData(xlfile, tssheet, j, 15);

							nusn2.city = xl.getCellData(xlfile, tssheet, j, 16);

							nusn2.state = xl.getCellData(xlfile, tssheet, j, 17);

							nusn2.zipcode = xl.getCellData(xlfile, tssheet, j, 18);

							nusn2.typeofaccount = xl.getCellData(xlfile, tssheet, j, 19);

							res = nusn2.NDIRAUsersignupNegativeStep2();
							String rese9 = d
									.findElement(
											By.xpath("html/body/div[3]/form/div[1]/div[2]/div[2]/div[1]/div[1]/label"))
									.getText();
							System.out.println(rese9);
							d.get(url);
							Sleeper.sleepTightInSeconds(5);
							d.findElement(By.xpath("html/body/div[1]/div/div[1]/div[1]/ul/li[6]/a/div[2]")).click();

							break;

						case "NDIRAUSERSIGNUPNEGATIVE10":
							nusn.fname = xl.getCellData(xlfile, tssheet, j, 5);

							nusn.lname = xl.getCellData(xlfile, tssheet, j, 6);

							nusn.email = xl.getCellData(xlfile, tssheet, j, 7);

							nusn.pass = xl.getCellData(xlfile, tssheet, j, 8);

							nusn.cpass = xl.getCellData(xlfile, tssheet, j, 9);

							nusn2.DOB = xl.getCellData(xlfile, tssheet, j, 10);

							nusn2.SSN = xl.getCellData(xlfile, tssheet, j, 11);

							nusn2.maritalstatus = xl.getCellData(xlfile, tssheet, j, 12);

							nusn2.phone = xl.getCellData(xlfile, tssheet, j, 13);

							nusn2.address1 = xl.getCellData(xlfile, tssheet, j, 14);

							nusn2.address2 = xl.getCellData(xlfile, tssheet, j, 15);

							nusn2.city = xl.getCellData(xlfile, tssheet, j, 16);

							nusn2.state = xl.getCellData(xlfile, tssheet, j, 17);

							nusn2.zipcode = xl.getCellData(xlfile, tssheet, j, 18);

							nusn2.typeofaccount = xl.getCellData(xlfile, tssheet, j, 19);

							res = nusn2.NDIRAUsersignupNegativeStep2();
							String rese10 = d
									.findElement(
											By.xpath("html/body/div[3]/form/div[1]/div[2]/div[2]/div[1]/div[1]/label"))
									.getText();
							System.out.println(rese10);
							d.get(url);
							Sleeper.sleepTightInSeconds(5);
							d.findElement(By.xpath("html/body/div[1]/div/div[1]/div[1]/ul/li[6]/a/div[2]")).click();

							break;

						case "NDIRAUSERSIGNUPNEGATIVE11":
							nusn.fname = xl.getCellData(xlfile, tssheet, j, 5);

							nusn.lname = xl.getCellData(xlfile, tssheet, j, 6);

							nusn.email = xl.getCellData(xlfile, tssheet, j, 7);

							nusn.pass = xl.getCellData(xlfile, tssheet, j, 8);

							nusn.cpass = xl.getCellData(xlfile, tssheet, j, 9);

							nusn2.DOB = xl.getCellData(xlfile, tssheet, j, 10);

							nusn2.SSN = xl.getCellData(xlfile, tssheet, j, 11);

							nusn2.maritalstatus = xl.getCellData(xlfile, tssheet, j, 12);

							nusn2.phone = xl.getCellData(xlfile, tssheet, j, 13);

							nusn2.address1 = xl.getCellData(xlfile, tssheet, j, 14);

							nusn2.address2 = xl.getCellData(xlfile, tssheet, j, 15);

							nusn2.city = xl.getCellData(xlfile, tssheet, j, 16);

							nusn2.state = xl.getCellData(xlfile, tssheet, j, 17);

							nusn2.zipcode = xl.getCellData(xlfile, tssheet, j, 18);

							nusn2.typeofaccount = xl.getCellData(xlfile, tssheet, j, 19);

							res = nusn2.NDIRAUsersignupNegativeStep2();
							String rese11 = d
									.findElement(
											By.xpath("html/body/div[3]/form/div[1]/div[2]/div[2]/div[1]/div[2]/label"))
									.getText();
							System.out.println(rese11);
							d.get(url);
							Sleeper.sleepTightInSeconds(5);
							d.findElement(By.xpath("html/body/div[1]/div/div[1]/div[1]/ul/li[6]/a/div[2]")).click();

							break;

						case "NDIRAUSERSIGNUPNEGATIVE12":
							nusn.fname = xl.getCellData(xlfile, tssheet, j, 5);

							nusn.lname = xl.getCellData(xlfile, tssheet, j, 6);

							nusn.email = xl.getCellData(xlfile, tssheet, j, 7);

							nusn.pass = xl.getCellData(xlfile, tssheet, j, 8);

							nusn.cpass = xl.getCellData(xlfile, tssheet, j, 9);

							nusn2.DOB = xl.getCellData(xlfile, tssheet, j, 10);

							nusn2.SSN = xl.getCellData(xlfile, tssheet, j, 11);

							nusn2.maritalstatus = xl.getCellData(xlfile, tssheet, j, 12);

							nusn2.phone = xl.getCellData(xlfile, tssheet, j, 13);

							nusn2.address1 = xl.getCellData(xlfile, tssheet, j, 14);

							nusn2.address2 = xl.getCellData(xlfile, tssheet, j, 15);

							nusn2.city = xl.getCellData(xlfile, tssheet, j, 16);

							nusn2.state = xl.getCellData(xlfile, tssheet, j, 17);

							nusn2.zipcode = xl.getCellData(xlfile, tssheet, j, 18);

							nusn2.typeofaccount = xl.getCellData(xlfile, tssheet, j, 19);

							res = nusn2.NDIRAUsersignupNegativeStep2();
							String rese12 = d
									.findElement(
											By.xpath("html/body/div[3]/form/div[1]/div[2]/div[2]/div[1]/div[2]/label"))
									.getText();
							System.out.println(rese12);
							d.get(url);
							Sleeper.sleepTightInSeconds(5);
							d.findElement(By.xpath("html/body/div[1]/div/div[1]/div[1]/ul/li[6]/a/div[2]")).click();

							break;

						case "NDIRAUSERSIGNUPNEGATIVE14":
							nusn.fname = xl.getCellData(xlfile, tssheet, j, 5);

							nusn.lname = xl.getCellData(xlfile, tssheet, j, 6);

							nusn.email = xl.getCellData(xlfile, tssheet, j, 7);

							nusn.pass = xl.getCellData(xlfile, tssheet, j, 8);

							nusn.cpass = xl.getCellData(xlfile, tssheet, j, 9);

							nusn2.DOB = xl.getCellData(xlfile, tssheet, j, 10);

							nusn2.SSN = xl.getCellData(xlfile, tssheet, j, 11);

							nusn2.maritalstatus = xl.getCellData(xlfile, tssheet, j, 12);

							nusn2.phone = xl.getCellData(xlfile, tssheet, j, 13);

							nusn2.address1 = xl.getCellData(xlfile, tssheet, j, 14);

							nusn2.address2 = xl.getCellData(xlfile, tssheet, j, 15);

							nusn2.city = xl.getCellData(xlfile, tssheet, j, 16);

							nusn2.state = xl.getCellData(xlfile, tssheet, j, 17);

							nusn2.zipcode = xl.getCellData(xlfile, tssheet, j, 18);

							nusn2.typeofaccount = xl.getCellData(xlfile, tssheet, j, 19);

							res = nusn2.NDIRAUsersignupNegativeStep2();
							String rese14 = d
									.findElement(
											By.xpath("html/body/div[3]/form/div[1]/div[2]/div[2]/div[2]/div[2]/label"))
									.getText();
							System.out.println(rese14);
							d.get(url);
							Sleeper.sleepTightInSeconds(5);
							d.findElement(By.xpath("html/body/div[1]/div/div[1]/div[1]/ul/li[6]/a/div[2]")).click();

							break;

						case "NDIRAUSERSIGNUPNEGATIVE15":
							nusn.fname = xl.getCellData(xlfile, tssheet, j, 5);

							nusn.lname = xl.getCellData(xlfile, tssheet, j, 6);

							nusn.email = xl.getCellData(xlfile, tssheet, j, 7);

							nusn.pass = xl.getCellData(xlfile, tssheet, j, 8);

							nusn.cpass = xl.getCellData(xlfile, tssheet, j, 9);

							nusn2.DOB = xl.getCellData(xlfile, tssheet, j, 10);

							nusn2.SSN = xl.getCellData(xlfile, tssheet, j, 11);

							nusn2.maritalstatus = xl.getCellData(xlfile, tssheet, j, 12);

							nusn2.phone = xl.getCellData(xlfile, tssheet, j, 13);

							nusn2.address1 = xl.getCellData(xlfile, tssheet, j, 14);

							nusn2.address2 = xl.getCellData(xlfile, tssheet, j, 15);

							nusn2.city = xl.getCellData(xlfile, tssheet, j, 16);

							nusn2.state = xl.getCellData(xlfile, tssheet, j, 17);

							nusn2.zipcode = xl.getCellData(xlfile, tssheet, j, 18);

							nusn2.typeofaccount = xl.getCellData(xlfile, tssheet, j, 19);

							res = nusn2.NDIRAUsersignupNegativeStep2();
							String rese15 = d
									.findElement(
											By.xpath("html/body/div[3]/form/div[1]/div[2]/div[2]/div[2]/div[2]/label"))
									.getText();
							System.out.println(rese15);
							d.get(url);
							Sleeper.sleepTightInSeconds(5);
							d.findElement(By.xpath("html/body/div[1]/div/div[1]/div[1]/ul/li[6]/a/div[2]")).click();

							break;

						case "NDIRAUSERSIGNUPNEGATIVE16":
							nusn.fname = xl.getCellData(xlfile, tssheet, j, 5);

							nusn.lname = xl.getCellData(xlfile, tssheet, j, 6);

							nusn.email = xl.getCellData(xlfile, tssheet, j, 7);

							nusn.pass = xl.getCellData(xlfile, tssheet, j, 8);

							nusn.cpass = xl.getCellData(xlfile, tssheet, j, 9);

							nusn2.DOB = xl.getCellData(xlfile, tssheet, j, 10);

							nusn2.SSN = xl.getCellData(xlfile, tssheet, j, 11);

							nusn2.maritalstatus = xl.getCellData(xlfile, tssheet, j, 12);

							nusn2.phone = xl.getCellData(xlfile, tssheet, j, 13);

							nusn2.address1 = xl.getCellData(xlfile, tssheet, j, 14);

							nusn2.address2 = xl.getCellData(xlfile, tssheet, j, 15);

							nusn2.city = xl.getCellData(xlfile, tssheet, j, 16);

							nusn2.state = xl.getCellData(xlfile, tssheet, j, 17);

							nusn2.zipcode = xl.getCellData(xlfile, tssheet, j, 18);

							nusn2.typeofaccount = xl.getCellData(xlfile, tssheet, j, 19);

							res = nusn2.NDIRAUsersignupNegativeStep2();
							String rese16 = d
									.findElement(
											By.xpath("html/body/div[3]/form/div[2]/div[2]/div[2]/div[1]/div[1]/label"))
									.getText();
							System.out.println(rese16);
							d.get(url);
							Sleeper.sleepTightInSeconds(5);
							d.findElement(By.xpath("html/body/div[1]/div/div[1]/div[1]/ul/li[6]/a/div[2]")).click();

							break;

						case "NDIRAUSERSIGNUPNEGATIVE17":
							nusn.fname = xl.getCellData(xlfile, tssheet, j, 5);

							nusn.lname = xl.getCellData(xlfile, tssheet, j, 6);

							nusn.email = xl.getCellData(xlfile, tssheet, j, 7);

							nusn.pass = xl.getCellData(xlfile, tssheet, j, 8);

							nusn.cpass = xl.getCellData(xlfile, tssheet, j, 9);

							nusn2.DOB = xl.getCellData(xlfile, tssheet, j, 10);

							nusn2.SSN = xl.getCellData(xlfile, tssheet, j, 11);

							nusn2.maritalstatus = xl.getCellData(xlfile, tssheet, j, 12);

							nusn2.phone = xl.getCellData(xlfile, tssheet, j, 13);

							nusn2.address1 = xl.getCellData(xlfile, tssheet, j, 14);

							nusn2.address2 = xl.getCellData(xlfile, tssheet, j, 15);

							nusn2.city = xl.getCellData(xlfile, tssheet, j, 16);

							nusn2.state = xl.getCellData(xlfile, tssheet, j, 17);

							nusn2.zipcode = xl.getCellData(xlfile, tssheet, j, 18);

							nusn2.typeofaccount = xl.getCellData(xlfile, tssheet, j, 19);

							res = nusn2.NDIRAUsersignupNegativeStep2();
							String rese17 = d
									.findElement(
											By.xpath("html/body/div[3]/form/div[2]/div[2]/div[2]/div[2]/div[1]/label"))
									.getText();
							System.out.println(rese17);
							d.get(url);
							Sleeper.sleepTightInSeconds(5);
							d.findElement(By.xpath("html/body/div[1]/div/div[1]/div[1]/ul/li[6]/a/div[2]")).click();

							break;

						case "NDIRAUSERSIGNUPNEGATIVE19":
							nusn.fname = xl.getCellData(xlfile, tssheet, j, 5);

							nusn.lname = xl.getCellData(xlfile, tssheet, j, 6);

							nusn.email = xl.getCellData(xlfile, tssheet, j, 7);

							nusn.pass = xl.getCellData(xlfile, tssheet, j, 8);

							nusn.cpass = xl.getCellData(xlfile, tssheet, j, 9);

							nusn2.DOB = xl.getCellData(xlfile, tssheet, j, 10);

							nusn2.SSN = xl.getCellData(xlfile, tssheet, j, 11);

							nusn2.maritalstatus = xl.getCellData(xlfile, tssheet, j, 12);

							nusn2.phone = xl.getCellData(xlfile, tssheet, j, 13);

							nusn2.address1 = xl.getCellData(xlfile, tssheet, j, 14);

							nusn2.address2 = xl.getCellData(xlfile, tssheet, j, 15);

							nusn2.city = xl.getCellData(xlfile, tssheet, j, 16);

							nusn2.state = xl.getCellData(xlfile, tssheet, j, 17);

							nusn2.zipcode = xl.getCellData(xlfile, tssheet, j, 18);

							nusn2.typeofaccount = xl.getCellData(xlfile, tssheet, j, 19);

							res = nusn2.NDIRAUsersignupNegativeStep2();
							String rese19 = d
									.findElement(
											By.xpath("html/body/div[3]/form/div[2]/div[2]/div[2]/div[2]/div[3]/label"))
									.getText();
							System.out.println(rese19);
							d.get(url);
							Sleeper.sleepTightInSeconds(5);
							d.findElement(By.xpath("html/body/div[1]/div/div[1]/div[1]/ul/li[6]/a/div[2]")).click();

							break;

						case "NDIRAUSERSIGNUPNEGATIVE20":
							nusn.fname = xl.getCellData(xlfile, tssheet, j, 5);

							nusn.lname = xl.getCellData(xlfile, tssheet, j, 6);

							nusn.email = xl.getCellData(xlfile, tssheet, j, 7);

							nusn.pass = xl.getCellData(xlfile, tssheet, j, 8);

							nusn.cpass = xl.getCellData(xlfile, tssheet, j, 9);

							nusn2.DOB = xl.getCellData(xlfile, tssheet, j, 10);

							nusn2.SSN = xl.getCellData(xlfile, tssheet, j, 11);

							nusn2.maritalstatus = xl.getCellData(xlfile, tssheet, j, 12);

							nusn2.phone = xl.getCellData(xlfile, tssheet, j, 13);

							nusn2.address1 = xl.getCellData(xlfile, tssheet, j, 14);

							nusn2.address2 = xl.getCellData(xlfile, tssheet, j, 15);

							nusn2.city = xl.getCellData(xlfile, tssheet, j, 16);

							nusn2.state = xl.getCellData(xlfile, tssheet, j, 17);

							nusn2.zipcode = xl.getCellData(xlfile, tssheet, j, 18);

							nusn2.typeofaccount = xl.getCellData(xlfile, tssheet, j, 19);

							res = nusn2.NDIRAUsersignupNegativeStep2();
							String rese20 = d
									.findElement(
											By.xpath("html/body/div[3]/form/div[2]/div[2]/div[2]/div[2]/div[3]/label"))
									.getText();
							System.out.println(rese20);
							d.get(url);
							Sleeper.sleepTightInSeconds(5);
							d.findElement(By.xpath("html/body/div[1]/div/div[1]/div[1]/ul/li[6]/a/div[2]")).click();

							break;

						case "NDIRAUSERSIGNUPNEGATIVE21":
							nusn.fname = xl.getCellData(xlfile, tssheet, j, 5);

							nusn.lname = xl.getCellData(xlfile, tssheet, j, 6);

							nusn.email = xl.getCellData(xlfile, tssheet, j, 7);

							nusn.pass = xl.getCellData(xlfile, tssheet, j, 8);

							nusn.cpass = xl.getCellData(xlfile, tssheet, j, 9);

							nusn2.DOB = xl.getCellData(xlfile, tssheet, j, 10);

							nusn2.SSN = xl.getCellData(xlfile, tssheet, j, 11);

							nusn2.maritalstatus = xl.getCellData(xlfile, tssheet, j, 12);

							nusn2.phone = xl.getCellData(xlfile, tssheet, j, 13);

							nusn2.address1 = xl.getCellData(xlfile, tssheet, j, 14);

							nusn2.address2 = xl.getCellData(xlfile, tssheet, j, 15);

							nusn2.city = xl.getCellData(xlfile, tssheet, j, 16);

							nusn2.state = xl.getCellData(xlfile, tssheet, j, 17);

							nusn2.zipcode = xl.getCellData(xlfile, tssheet, j, 18);

							nusn2.typeofaccount = xl.getCellData(xlfile, tssheet, j, 19);

							res = nusn2.NDIRAUsersignupNegativeStep2();
							String rese21 = d
									.findElement(
											By.xpath("html/body/div[3]/form/div[2]/div[2]/div[2]/div[2]/div[3]/label"))
									.getText();
							System.out.println(rese21);
							d.get(url);
							Sleeper.sleepTightInSeconds(5);
							d.findElement(By.xpath("html/body/div[1]/div/div[1]/div[1]/ul/li[6]/a/div[2]")).click();

							break;

						case "NDIRAUSERSIGNUPNEGATIVE22":
							nusn.fname = xl.getCellData(xlfile, tssheet, j, 5);

							nusn.lname = xl.getCellData(xlfile, tssheet, j, 6);

							nusn.email = xl.getCellData(xlfile, tssheet, j, 7);

							nusn.pass = xl.getCellData(xlfile, tssheet, j, 8);

							nusn.cpass = xl.getCellData(xlfile, tssheet, j, 9);

							nusn2.DOB = xl.getCellData(xlfile, tssheet, j, 10);

							nusn2.SSN = xl.getCellData(xlfile, tssheet, j, 11);

							nusn2.maritalstatus = xl.getCellData(xlfile, tssheet, j, 12);

							nusn2.phone = xl.getCellData(xlfile, tssheet, j, 13);

							nusn2.address1 = xl.getCellData(xlfile, tssheet, j, 14);

							nusn2.address2 = xl.getCellData(xlfile, tssheet, j, 15);

							nusn2.city = xl.getCellData(xlfile, tssheet, j, 16);

							nusn2.state = xl.getCellData(xlfile, tssheet, j, 17);

							nusn2.zipcode = xl.getCellData(xlfile, tssheet, j, 18);

							nusn2.typeofaccount = xl.getCellData(xlfile, tssheet, j, 19);

							nusn3.cardno = xl.getCellData(xlfile, tssheet, j, 20);

							nusn3.cardExpiryDate = xl.getCellData(xlfile, tssheet, j, 21);

							nusn3.cardName = xl.getCellData(xlfile, tssheet, j, 22);

							nusn3.cardCVCNumber = xl.getCellData(xlfile, tssheet, j, 23);

							res = nusn3.NDIRAUsersignupNegativeStep3();
							String rese22 = d
									.findElement(By
											.xpath("html/body/div[3]/form/div[3]/div/div[2]/div[2]/div[1]/div[1]/label"))
									.getText();
							System.out.println(rese22);
							d.get(url);
							Sleeper.sleepTightInSeconds(5);
							d.findElement(By.xpath("html/body/div[1]/div/div[1]/div[1]/ul/li[6]/a/div[2]")).click();

							break;

						case "NDIRAUSERSIGNUPNEGATIVE23":
							nusn.fname = xl.getCellData(xlfile, tssheet, j, 5);

							nusn.lname = xl.getCellData(xlfile, tssheet, j, 6);

							nusn.email = xl.getCellData(xlfile, tssheet, j, 7);

							nusn.pass = xl.getCellData(xlfile, tssheet, j, 8);

							nusn.cpass = xl.getCellData(xlfile, tssheet, j, 9);

							nusn2.DOB = xl.getCellData(xlfile, tssheet, j, 10);

							nusn2.SSN = xl.getCellData(xlfile, tssheet, j, 11);

							nusn2.maritalstatus = xl.getCellData(xlfile, tssheet, j, 12);

							nusn2.phone = xl.getCellData(xlfile, tssheet, j, 13);

							nusn2.address1 = xl.getCellData(xlfile, tssheet, j, 14);

							nusn2.address2 = xl.getCellData(xlfile, tssheet, j, 15);

							nusn2.city = xl.getCellData(xlfile, tssheet, j, 16);

							nusn2.state = xl.getCellData(xlfile, tssheet, j, 17);

							nusn2.zipcode = xl.getCellData(xlfile, tssheet, j, 18);

							nusn2.typeofaccount = xl.getCellData(xlfile, tssheet, j, 19);

							nusn3.cardno = xl.getCellData(xlfile, tssheet, j, 20);

							nusn3.cardExpiryDate = xl.getCellData(xlfile, tssheet, j, 21);

							nusn3.cardName = xl.getCellData(xlfile, tssheet, j, 22);

							nusn3.cardCVCNumber = xl.getCellData(xlfile, tssheet, j, 23);

							res = nusn3.NDIRAUsersignupNegativeStep3();
							String rese23 = d
									.findElement(By
											.xpath("html/body/div[3]/form/div[3]/div/div[2]/div[2]/div[1]/div[1]/label"))
									.getText();
							System.out.println(rese23);
							d.get(url);
							Sleeper.sleepTightInSeconds(5);
							d.findElement(By.xpath("html/body/div[1]/div/div[1]/div[1]/ul/li[6]/a/div[2]")).click();

							break;

						case "NDIRAUSERSIGNUPNEGATIVE24":
							nusn.fname = xl.getCellData(xlfile, tssheet, j, 5);

							nusn.lname = xl.getCellData(xlfile, tssheet, j, 6);

							nusn.email = xl.getCellData(xlfile, tssheet, j, 7);

							nusn.pass = xl.getCellData(xlfile, tssheet, j, 8);

							nusn.cpass = xl.getCellData(xlfile, tssheet, j, 9);

							nusn2.DOB = xl.getCellData(xlfile, tssheet, j, 10);

							nusn2.SSN = xl.getCellData(xlfile, tssheet, j, 11);

							nusn2.maritalstatus = xl.getCellData(xlfile, tssheet, j, 12);

							nusn2.phone = xl.getCellData(xlfile, tssheet, j, 13);

							nusn2.address1 = xl.getCellData(xlfile, tssheet, j, 14);

							nusn2.address2 = xl.getCellData(xlfile, tssheet, j, 15);

							nusn2.city = xl.getCellData(xlfile, tssheet, j, 16);

							nusn2.state = xl.getCellData(xlfile, tssheet, j, 17);

							nusn2.zipcode = xl.getCellData(xlfile, tssheet, j, 18);

							nusn2.typeofaccount = xl.getCellData(xlfile, tssheet, j, 19);

							nusn3.cardno = xl.getCellData(xlfile, tssheet, j, 20);

							nusn3.cardExpiryDate = xl.getCellData(xlfile, tssheet, j, 21);

							nusn3.cardName = xl.getCellData(xlfile, tssheet, j, 22);

							nusn3.cardCVCNumber = xl.getCellData(xlfile, tssheet, j, 23);

							res = nusn3.NDIRAUsersignupNegativeStep3();
							String rese24 = d
									.findElement(By
											.xpath("html/body/div[3]/form/div[3]/div/div[2]/div[2]/div[1]/div[2]/label"))
									.getText();
							System.out.println(rese24);
							d.get(url);
							Sleeper.sleepTightInSeconds(5);
							d.findElement(By.xpath("html/body/div[1]/div/div[1]/div[1]/ul/li[6]/a/div[2]")).click();

							break;

						case "NDIRAUSERSIGNUPNEGATIVE25":
							nusn.fname = xl.getCellData(xlfile, tssheet, j, 5);

							nusn.lname = xl.getCellData(xlfile, tssheet, j, 6);

							nusn.email = xl.getCellData(xlfile, tssheet, j, 7);

							nusn.pass = xl.getCellData(xlfile, tssheet, j, 8);

							nusn.cpass = xl.getCellData(xlfile, tssheet, j, 9);

							nusn2.DOB = xl.getCellData(xlfile, tssheet, j, 10);

							nusn2.SSN = xl.getCellData(xlfile, tssheet, j, 11);

							nusn2.maritalstatus = xl.getCellData(xlfile, tssheet, j, 12);

							nusn2.phone = xl.getCellData(xlfile, tssheet, j, 13);

							nusn2.address1 = xl.getCellData(xlfile, tssheet, j, 14);

							nusn2.address2 = xl.getCellData(xlfile, tssheet, j, 15);

							nusn2.city = xl.getCellData(xlfile, tssheet, j, 16);

							nusn2.state = xl.getCellData(xlfile, tssheet, j, 17);

							nusn2.zipcode = xl.getCellData(xlfile, tssheet, j, 18);

							nusn2.typeofaccount = xl.getCellData(xlfile, tssheet, j, 19);

							nusn3.cardno = xl.getCellData(xlfile, tssheet, j, 20);

							nusn3.cardExpiryDate = xl.getCellData(xlfile, tssheet, j, 21);

							nusn3.cardName = xl.getCellData(xlfile, tssheet, j, 22);

							nusn3.cardCVCNumber = xl.getCellData(xlfile, tssheet, j, 23);

							res = nusn3.NDIRAUsersignupNegativeStep3();
							String rese25 = d
									.findElement(By
											.xpath("html/body/div[3]/form/div[3]/div/div[2]/div[2]/div[1]/div[2]/label"))
									.getText();
							System.out.println(rese25);
							d.get(url);
							Sleeper.sleepTightInSeconds(5);
							d.findElement(By.xpath("html/body/div[1]/div/div[1]/div[1]/ul/li[6]/a/div[2]")).click();

							break;

						case "NDIRAUSERSIGNUPNEGATIVE26":
							nusn.fname = xl.getCellData(xlfile, tssheet, j, 5);

							nusn.lname = xl.getCellData(xlfile, tssheet, j, 6);

							nusn.email = xl.getCellData(xlfile, tssheet, j, 7);

							nusn.pass = xl.getCellData(xlfile, tssheet, j, 8);

							nusn.cpass = xl.getCellData(xlfile, tssheet, j, 9);

							nusn2.DOB = xl.getCellData(xlfile, tssheet, j, 10);

							nusn2.SSN = xl.getCellData(xlfile, tssheet, j, 11);

							nusn2.maritalstatus = xl.getCellData(xlfile, tssheet, j, 12);

							nusn2.phone = xl.getCellData(xlfile, tssheet, j, 13);

							nusn2.address1 = xl.getCellData(xlfile, tssheet, j, 14);

							nusn2.address2 = xl.getCellData(xlfile, tssheet, j, 15);

							nusn2.city = xl.getCellData(xlfile, tssheet, j, 16);

							nusn2.state = xl.getCellData(xlfile, tssheet, j, 17);

							nusn2.zipcode = xl.getCellData(xlfile, tssheet, j, 18);

							nusn2.typeofaccount = xl.getCellData(xlfile, tssheet, j, 19);

							nusn3.cardno = xl.getCellData(xlfile, tssheet, j, 20);

							nusn3.cardExpiryDate = xl.getCellData(xlfile, tssheet, j, 21);

							nusn3.cardName = xl.getCellData(xlfile, tssheet, j, 22);

							nusn3.cardCVCNumber = xl.getCellData(xlfile, tssheet, j, 23);

							res = nusn3.NDIRAUsersignupNegativeStep3();
							String rese26 = d
									.findElement(By
											.xpath("html/body/div[3]/form/div[3]/div/div[2]/div[2]/div[2]/div[1]/label"))
									.getText();
							System.out.println(rese26);
							d.get(url);
							Sleeper.sleepTightInSeconds(5);
							d.findElement(By.xpath("html/body/div[1]/div/div[1]/div[1]/ul/li[6]/a/div[2]")).click();

							break;

						case "NDIRAUSERSIGNUPNEGATIVE27":
							nusn.fname = xl.getCellData(xlfile, tssheet, j, 5);

							nusn.lname = xl.getCellData(xlfile, tssheet, j, 6);

							nusn.email = xl.getCellData(xlfile, tssheet, j, 7);

							nusn.pass = xl.getCellData(xlfile, tssheet, j, 8);

							nusn.cpass = xl.getCellData(xlfile, tssheet, j, 9);

							nusn2.DOB = xl.getCellData(xlfile, tssheet, j, 10);

							nusn2.SSN = xl.getCellData(xlfile, tssheet, j, 11);

							nusn2.maritalstatus = xl.getCellData(xlfile, tssheet, j, 12);

							nusn2.phone = xl.getCellData(xlfile, tssheet, j, 13);

							nusn2.address1 = xl.getCellData(xlfile, tssheet, j, 14);

							nusn2.address2 = xl.getCellData(xlfile, tssheet, j, 15);

							nusn2.city = xl.getCellData(xlfile, tssheet, j, 16);

							nusn2.state = xl.getCellData(xlfile, tssheet, j, 17);

							nusn2.zipcode = xl.getCellData(xlfile, tssheet, j, 18);

							nusn2.typeofaccount = xl.getCellData(xlfile, tssheet, j, 19);

							nusn3.cardno = xl.getCellData(xlfile, tssheet, j, 20);

							nusn3.cardExpiryDate = xl.getCellData(xlfile, tssheet, j, 21);

							nusn3.cardName = xl.getCellData(xlfile, tssheet, j, 22);

							nusn3.cardCVCNumber = xl.getCellData(xlfile, tssheet, j, 23);

							res = nusn3.NDIRAUsersignupNegativeStep3();
							String rese27 = d
									.findElement(By
											.xpath("html/body/div[3]/form/div[3]/div/div[2]/div[2]/div[2]/div[2]/label"))
									.getText();
							System.out.println(rese27);
							d.get(url);
							Sleeper.sleepTightInSeconds(5);
							d.findElement(By.xpath("html/body/div[1]/div/div[1]/div[1]/ul/li[6]/a/div[2]")).click();

							break;

						case "NDIRAUSERSIGNUPNEGATIVE28":
							nusn.fname = xl.getCellData(xlfile, tssheet, j, 5);

							nusn.lname = xl.getCellData(xlfile, tssheet, j, 6);

							nusn.email = xl.getCellData(xlfile, tssheet, j, 7);

							nusn.pass = xl.getCellData(xlfile, tssheet, j, 8);

							nusn.cpass = xl.getCellData(xlfile, tssheet, j, 9);

							nusn2.DOB = xl.getCellData(xlfile, tssheet, j, 10);

							nusn2.SSN = xl.getCellData(xlfile, tssheet, j, 11);

							nusn2.maritalstatus = xl.getCellData(xlfile, tssheet, j, 12);

							nusn2.phone = xl.getCellData(xlfile, tssheet, j, 13);

							nusn2.address1 = xl.getCellData(xlfile, tssheet, j, 14);

							nusn2.address2 = xl.getCellData(xlfile, tssheet, j, 15);

							nusn2.city = xl.getCellData(xlfile, tssheet, j, 16);

							nusn2.state = xl.getCellData(xlfile, tssheet, j, 17);

							nusn2.zipcode = xl.getCellData(xlfile, tssheet, j, 18);

							nusn2.typeofaccount = xl.getCellData(xlfile, tssheet, j, 19);

							nusn3.cardno = xl.getCellData(xlfile, tssheet, j, 20);

							nusn3.cardExpiryDate = xl.getCellData(xlfile, tssheet, j, 21);

							nusn3.cardName = xl.getCellData(xlfile, tssheet, j, 22);

							nusn3.cardCVCNumber = xl.getCellData(xlfile, tssheet, j, 23);

							res = nusn3.NDIRAUsersignupNegativeStep3();
							String rese28 = d
									.findElement(By
											.xpath("html/body/div[3]/form/div[3]/div/div[2]/div[2]/div[2]/div[2]/label"))
									.getText();
							System.out.println(rese28);
							d.get(url);
							Sleeper.sleepTightInSeconds(5);
							d.findElement(By.xpath("html/body/div[1]/div/div[1]/div[1]/ul/li[6]/a/div[2]")).click();
							break;

						case "SPONSORREGULARLOGINLOGOUT":
							srlo.uname = xl.getCellData(xlfile, tssheet, j, 5);
							srlo.pass = xl.getCellData(xlfile, tssheet, j, 6);
							res = srlo.SponsorRegularLoginLogout();
							break;

						case "FORGETPASSWORDNEGATIVE":
							fpn.email = xl.getCellData(xlfile, tssheet, j, 5);
							res = fpn.ForgetPasswordNegative();
							d.get(url);
							break;

						case "ADDINVESTMENTNEGATIVE":
							ain.Investment = xl.getCellData(xlfile, tssheet, j, 5);
							ain.Type = xl.getCellData(xlfile, tssheet, j, 6);
							ain.Description = xl.getCellData(xlfile, tssheet, j, 7);
							res = ain.AddInvestmentNegative();
							slog.Logout();
							break;

						case "ADDINVESTMENTNEGATIVE1":
							ain.Investment = xl.getCellData(xlfile, tssheet, j, 5);
							ain.Type = xl.getCellData(xlfile, tssheet, j, 6);
							ain.Description = xl.getCellData(xlfile, tssheet, j, 7);
							res = ain.AddInvestmentNegative();
							slog.Logout();
							break;

						case "ADDINVESTMENTNEGATIVE2":
							ain.Investment = xl.getCellData(xlfile, tssheet, j, 5);
							ain.Type = xl.getCellData(xlfile, tssheet, j, 6);
							ain.Description = xl.getCellData(xlfile, tssheet, j, 7);
							res = ain.AddInvestmentNegative();
							slog.Logout();
							break;

						case "ADDINVESTMENTNEGATIVE3":
							ain.Investment = xl.getCellData(xlfile, tssheet, j, 5);
							ain.Type = xl.getCellData(xlfile, tssheet, j, 6);
							ain.Description = xl.getCellData(xlfile, tssheet, j, 7);
							res = ain.AddInvestmentNegative();
							slog.Logout();
							break;

						case "MANAGETEAMNEGATIVE":
							mtn.fname = xl.getCellData(xlfile, tssheet, j, 5);
							mtn.lname = xl.getCellData(xlfile, tssheet, j, 6);
							mtn.email = xl.getCellData(xlfile, tssheet, j, 7);
							res = mtn.ManageTeamNegative();
							slog.Logout();
							break;

						case "MANAGETEAMNEGATIVE1":
							mtn.fname = xl.getCellData(xlfile, tssheet, j, 5);
							mtn.lname = xl.getCellData(xlfile, tssheet, j, 6);
							mtn.email = xl.getCellData(xlfile, tssheet, j, 7);
							res = mtn.ManageTeamNegative();
							slog.Logout();
							break;

						case "MANAGETEAMNEGATIVE2":
							mtn.fname = xl.getCellData(xlfile, tssheet, j, 5);
							mtn.lname = xl.getCellData(xlfile, tssheet, j, 6);
							mtn.email = xl.getCellData(xlfile, tssheet, j, 7);
							res = mtn.ManageTeamNegative();
							slog.Logout();
							break;

						case "MANAGETEAMNEGATIVE3":
							mtn.fname = xl.getCellData(xlfile, tssheet, j, 5);
							mtn.lname = xl.getCellData(xlfile, tssheet, j, 6);
							mtn.email = xl.getCellData(xlfile, tssheet, j, 7);
							res = mtn.ManageTeamNegative();
							slog.Logout();
							break;

						case "MANAGETEAMNEGATIVE4":
							mtn.fname = xl.getCellData(xlfile, tssheet, j, 5);
							mtn.lname = xl.getCellData(xlfile, tssheet, j, 6);
							mtn.email = xl.getCellData(xlfile, tssheet, j, 7);
							res = mtn.ManageTeamNegative();
							slog.Logout();
							break;

						case "MANAGETEAMNEGATIVE5":
							mtn.fname = xl.getCellData(xlfile, tssheet, j, 5);
							mtn.lname = xl.getCellData(xlfile, tssheet, j, 6);
							mtn.email = xl.getCellData(xlfile, tssheet, j, 7);
							res = mtn.ManageTeamNegative();
							slog.Logout();
							break;

						case "MANAGEREMOVETEAMMEMBERNEGATIVE":
							mremn.fname = xl.getCellData(xlfile, tssheet, j, 5);
							mremn.lname = xl.getCellData(xlfile, tssheet, j, 6);
							mremn.email = xl.getCellData(xlfile, tssheet, j, 7);
							mremn.email1 = xl.getCellData(xlfile, tssheet, j, 8);
							res = mremn.ManageRemoveTeamMemberNegative();
							slog.Logout();
							break;

						case "SPONSORMESSAGENEGATIVE":
							res = smn.SponsorMessageNegative();
							slog.Logout();
							break;

						case "SPONSORUSERSIGNUPNEGATIVE":
							sun1.fname = xl.getCellData(xlfile, tssheet, j, 5);

							sun1.lname = xl.getCellData(xlfile, tssheet, j, 6);

							sun1.email = xl.getCellData(xlfile, tssheet, j, 7);

							sun1.pass = xl.getCellData(xlfile, tssheet, j, 8);

							sun1.cpass = xl.getCellData(xlfile, tssheet, j, 9);

							res = sun1.SponsorUsersignupNegativeStep1();

							String Srese = d
									.findElement(
											By.xpath("html/body/div[3]/form/div[1]/div[2]/div[2]/div/div[1]/label"))
									.getText();
							System.out.println(Srese);

							d.get(url);
							Sleeper.sleepTightInSeconds(5);
							d.findElement(By.xpath("html/body/div[1]/div/div[1]/div/div/div[3]/ul/li[2]/a")).click();

							break;

						case "SPONSORUSERSIGNUPNEGATIVE1":
							sun1.fname = xl.getCellData(xlfile, tssheet, j, 5);

							sun1.lname = xl.getCellData(xlfile, tssheet, j, 6);

							sun1.email = xl.getCellData(xlfile, tssheet, j, 7);

							sun1.pass = xl.getCellData(xlfile, tssheet, j, 8);

							sun1.cpass = xl.getCellData(xlfile, tssheet, j, 9);

							res = sun1.SponsorUsersignupNegativeStep1();

							String Srese1 = d
									.findElement(
											By.xpath("html/body/div[3]/form/div[1]/div[2]/div[2]/div/div[3]/label"))
									.getText();
							System.out.println(Srese1);

							d.get(url);
							Sleeper.sleepTightInSeconds(5);
							d.findElement(By.xpath("html/body/div[1]/div/div[1]/div/div/div[3]/ul/li[2]/a")).click();

							break;

						case "SPONSORUSERSIGNUPNEGATIVE2":
							sun1.fname = xl.getCellData(xlfile, tssheet, j, 5);

							sun1.lname = xl.getCellData(xlfile, tssheet, j, 6);

							sun1.email = xl.getCellData(xlfile, tssheet, j, 7);

							sun1.pass = xl.getCellData(xlfile, tssheet, j, 8);

							sun1.cpass = xl.getCellData(xlfile, tssheet, j, 9);

							res = sun1.SponsorUsersignupNegativeStep1();

							String Srese2 = d
									.findElement(By.xpath("html/body/div[3]/form/div[2]/div[2]/div[2]/label[1]"))
									.getText();
							System.out.println(Srese2);

							d.get(url);
							Sleeper.sleepTightInSeconds(5);
							d.findElement(By.xpath("html/body/div[1]/div/div[1]/div/div/div[3]/ul/li[2]/a")).click();

							break;

						case "SPONSORUSERSIGNUPNEGATIVE3":
							sun1.fname = xl.getCellData(xlfile, tssheet, j, 5);

							sun1.lname = xl.getCellData(xlfile, tssheet, j, 6);

							sun1.email = xl.getCellData(xlfile, tssheet, j, 7);

							sun1.pass = xl.getCellData(xlfile, tssheet, j, 8);

							sun1.cpass = xl.getCellData(xlfile, tssheet, j, 9);

							res = sun1.SponsorUsersignupNegativeStep1();

							String Srese3 = d
									.findElement(By.xpath("html/body/div[3]/form/div[2]/div[2]/div[2]/label[1]"))
									.getText();
							System.out.println(Srese3);

							d.get(url);
							Sleeper.sleepTightInSeconds(5);
							d.findElement(By.xpath("html/body/div[1]/div/div[1]/div/div/div[3]/ul/li[2]/a")).click();

							break;

						case "SPONSORUSERSIGNUPNEGATIVE4":
							sun1.fname = xl.getCellData(xlfile, tssheet, j, 5);

							sun1.lname = xl.getCellData(xlfile, tssheet, j, 6);

							sun1.email = xl.getCellData(xlfile, tssheet, j, 7);

							sun1.pass = xl.getCellData(xlfile, tssheet, j, 8);

							sun1.cpass = xl.getCellData(xlfile, tssheet, j, 9);

							res = sun1.SponsorUsersignupNegativeStep1();

							String Srese4 = d
									.findElement(By.xpath("html/body/div[3]/form/div[2]/div[2]/div[2]/label[2]"))
									.getText();
							System.out.println(Srese4);

							d.get(url);
							Sleeper.sleepTightInSeconds(5);
							d.findElement(By.xpath("html/body/div[1]/div/div[1]/div/div/div[3]/ul/li[2]/a")).click();

							break;

						case "SPONSORUSERSIGNUPNEGATIVE5":
							sun1.fname = xl.getCellData(xlfile, tssheet, j, 5);

							sun1.lname = xl.getCellData(xlfile, tssheet, j, 6);

							sun1.email = xl.getCellData(xlfile, tssheet, j, 7);

							sun1.pass = xl.getCellData(xlfile, tssheet, j, 8);

							sun1.cpass = xl.getCellData(xlfile, tssheet, j, 9);

							res = sun1.SponsorUsersignupNegativeStep1();

							String Srese5 = d
									.findElement(By.xpath("html/body/div[3]/form/div[2]/div[2]/div[2]/label[2]"))
									.getText();
							System.out.println(Srese5);

							d.get(url);
							Sleeper.sleepTightInSeconds(5);
							d.findElement(By.xpath("html/body/div[1]/div/div[1]/div/div/div[3]/ul/li[2]/a")).click();

							break;

						case "SPONSORUSERSIGNUPNEGATIVE6":
							sun1.fname = xl.getCellData(xlfile, tssheet, j, 5);

							sun1.lname = xl.getCellData(xlfile, tssheet, j, 6);

							sun1.email = xl.getCellData(xlfile, tssheet, j, 7);

							sun1.pass = xl.getCellData(xlfile, tssheet, j, 8);

							sun1.cpass = xl.getCellData(xlfile, tssheet, j, 9);

							res = sun1.SponsorUsersignupNegativeStep1();

							String Srese6 = d
									.findElement(By.xpath("html/body/div[3]/form/div[2]/div[2]/div[2]/label[3]"))
									.getText();
							System.out.println(Srese6);

							d.get(url);
							Sleeper.sleepTightInSeconds(5);
							d.findElement(By.xpath("html/body/div[1]/div/div[1]/div/div/div[3]/ul/li[2]/a")).click();

							break;

						case "SPONSORUSERSIGNUPNEGATIVE7":
							sun1.fname = xl.getCellData(xlfile, tssheet, j, 5);

							sun1.lname = xl.getCellData(xlfile, tssheet, j, 6);

							sun1.email = xl.getCellData(xlfile, tssheet, j, 7);

							sun1.pass = xl.getCellData(xlfile, tssheet, j, 8);

							sun1.cpass = xl.getCellData(xlfile, tssheet, j, 9);

							res = sun1.SponsorUsersignupNegativeStep1();

							Sleeper.sleepTightInSeconds(8);

							String Srese7 = d.findElement(By.xpath("html/body/div[3]/form/div[2]/div[2]/div[2]/label"))
									.getText();
							System.out.println(Srese7);

							d.get(url);
							Sleeper.sleepTightInSeconds(5);
							d.findElement(By.xpath("html/body/div[1]/div/div[1]/div/div/div[3]/ul/li[2]/a")).click();

							break;

						case "SPONSORUSERSIGNUPNEGATIVE8":
							sun1.fname = xl.getCellData(xlfile, tssheet, j, 5);

							sun1.lname = xl.getCellData(xlfile, tssheet, j, 6);

							sun1.email = xl.getCellData(xlfile, tssheet, j, 7);

							sun1.pass = xl.getCellData(xlfile, tssheet, j, 8);

							sun1.cpass = xl.getCellData(xlfile, tssheet, j, 9);

							sun2.DOB = xl.getCellData(xlfile, tssheet, j, 10);

							sun2.SSN = xl.getCellData(xlfile, tssheet, j, 11);

							sun2.maritalstatus = xl.getCellData(xlfile, tssheet, j, 12);

							sun2.phone = xl.getCellData(xlfile, tssheet, j, 13);

							sun2.address1 = xl.getCellData(xlfile, tssheet, j, 14);

							sun2.address2 = xl.getCellData(xlfile, tssheet, j, 15);

							sun2.city = xl.getCellData(xlfile, tssheet, j, 16);

							sun2.state = xl.getCellData(xlfile, tssheet, j, 17);

							sun2.zipcode = xl.getCellData(xlfile, tssheet, j, 18);

							sun2.typeofaccount = xl.getCellData(xlfile, tssheet, j, 19);

							res = sun2.SponsorUsersignupNegativeStep2();
							String Srese8 = d
									.findElement(
											By.xpath("html/body/div[3]/form/div[1]/div[2]/div[2]/div[1]/div[1]/label"))
									.getText();
							System.out.println(Srese8);
							d.get(url);
							Sleeper.sleepTightInSeconds(5);
							d.findElement(By.xpath("html/body/div[1]/div/div[1]/div/div/div[3]/ul/li[2]/a")).click();
							break;

						case "SPONSORUSERSIGNUPNEGATIVE9":
							sun1.fname = xl.getCellData(xlfile, tssheet, j, 5);

							sun1.lname = xl.getCellData(xlfile, tssheet, j, 6);

							sun1.email = xl.getCellData(xlfile, tssheet, j, 7);

							sun1.pass = xl.getCellData(xlfile, tssheet, j, 8);

							sun1.cpass = xl.getCellData(xlfile, tssheet, j, 9);

							sun2.DOB = xl.getCellData(xlfile, tssheet, j, 10);

							sun2.SSN = xl.getCellData(xlfile, tssheet, j, 11);

							sun2.maritalstatus = xl.getCellData(xlfile, tssheet, j, 12);

							sun2.phone = xl.getCellData(xlfile, tssheet, j, 13);

							sun2.address1 = xl.getCellData(xlfile, tssheet, j, 14);

							sun2.address2 = xl.getCellData(xlfile, tssheet, j, 15);

							sun2.city = xl.getCellData(xlfile, tssheet, j, 16);

							sun2.state = xl.getCellData(xlfile, tssheet, j, 17);

							sun2.zipcode = xl.getCellData(xlfile, tssheet, j, 18);

							sun2.typeofaccount = xl.getCellData(xlfile, tssheet, j, 19);

							res = sun2.SponsorUsersignupNegativeStep2();
							String Srese9 = d
									.findElement(
											By.xpath("html/body/div[3]/form/div[1]/div[2]/div[2]/div[1]/div[1]/label"))
									.getText();
							System.out.println(Srese9);
							d.get(url);
							Sleeper.sleepTightInSeconds(5);
							d.findElement(By.xpath("html/body/div[1]/div/div[1]/div/div/div[3]/ul/li[2]/a")).click();
							break;

						case "SPONSORUSERSIGNUPNEGATIVE10":
							sun1.fname = xl.getCellData(xlfile, tssheet, j, 5);

							sun1.lname = xl.getCellData(xlfile, tssheet, j, 6);

							sun1.email = xl.getCellData(xlfile, tssheet, j, 7);

							sun1.pass = xl.getCellData(xlfile, tssheet, j, 8);

							sun1.cpass = xl.getCellData(xlfile, tssheet, j, 9);

							sun2.DOB = xl.getCellData(xlfile, tssheet, j, 10);

							sun2.SSN = xl.getCellData(xlfile, tssheet, j, 11);

							sun2.maritalstatus = xl.getCellData(xlfile, tssheet, j, 12);

							sun2.phone = xl.getCellData(xlfile, tssheet, j, 13);

							sun2.address1 = xl.getCellData(xlfile, tssheet, j, 14);

							sun2.address2 = xl.getCellData(xlfile, tssheet, j, 15);

							sun2.city = xl.getCellData(xlfile, tssheet, j, 16);

							sun2.state = xl.getCellData(xlfile, tssheet, j, 17);

							sun2.zipcode = xl.getCellData(xlfile, tssheet, j, 18);

							sun2.typeofaccount = xl.getCellData(xlfile, tssheet, j, 19);

							res = sun2.SponsorUsersignupNegativeStep2();
							String Srese10 = d
									.findElement(
											By.xpath("html/body/div[3]/form/div[1]/div[2]/div[2]/div[1]/div[1]/label"))
									.getText();
							System.out.println(Srese10);
							d.get(url);
							Sleeper.sleepTightInSeconds(5);
							d.findElement(By.xpath("html/body/div[1]/div/div[1]/div/div/div[3]/ul/li[2]/a")).click();
							break;

						case "SPONSORUSERSIGNUPNEGATIVE11":
							sun1.fname = xl.getCellData(xlfile, tssheet, j, 5);

							sun1.lname = xl.getCellData(xlfile, tssheet, j, 6);

							sun1.email = xl.getCellData(xlfile, tssheet, j, 7);

							sun1.pass = xl.getCellData(xlfile, tssheet, j, 8);

							sun1.cpass = xl.getCellData(xlfile, tssheet, j, 9);

							sun2.DOB = xl.getCellData(xlfile, tssheet, j, 10);

							sun2.SSN = xl.getCellData(xlfile, tssheet, j, 11);

							sun2.maritalstatus = xl.getCellData(xlfile, tssheet, j, 12);

							sun2.phone = xl.getCellData(xlfile, tssheet, j, 13);

							sun2.address1 = xl.getCellData(xlfile, tssheet, j, 14);

							sun2.address2 = xl.getCellData(xlfile, tssheet, j, 15);

							sun2.city = xl.getCellData(xlfile, tssheet, j, 16);

							sun2.state = xl.getCellData(xlfile, tssheet, j, 17);

							sun2.zipcode = xl.getCellData(xlfile, tssheet, j, 18);

							sun2.typeofaccount = xl.getCellData(xlfile, tssheet, j, 19);

							res = sun2.SponsorUsersignupNegativeStep2();
							String Srese11 = d
									.findElement(
											By.xpath("html/body/div[3]/form/div[1]/div[2]/div[2]/div[1]/div[2]/label"))
									.getText();
							System.out.println(Srese11);
							d.get(url);
							Sleeper.sleepTightInSeconds(5);
							d.findElement(By.xpath("html/body/div[1]/div/div[1]/div/div/div[3]/ul/li[2]/a")).click();
							break;

						case "SPONSORUSERSIGNUPNEGATIVE12":
							sun1.fname = xl.getCellData(xlfile, tssheet, j, 5);

							sun1.lname = xl.getCellData(xlfile, tssheet, j, 6);

							sun1.email = xl.getCellData(xlfile, tssheet, j, 7);

							sun1.pass = xl.getCellData(xlfile, tssheet, j, 8);

							sun1.cpass = xl.getCellData(xlfile, tssheet, j, 9);

							sun2.DOB = xl.getCellData(xlfile, tssheet, j, 10);

							sun2.SSN = xl.getCellData(xlfile, tssheet, j, 11);

							sun2.maritalstatus = xl.getCellData(xlfile, tssheet, j, 12);

							sun2.phone = xl.getCellData(xlfile, tssheet, j, 13);

							sun2.address1 = xl.getCellData(xlfile, tssheet, j, 14);

							sun2.address2 = xl.getCellData(xlfile, tssheet, j, 15);

							sun2.city = xl.getCellData(xlfile, tssheet, j, 16);

							sun2.state = xl.getCellData(xlfile, tssheet, j, 17);

							sun2.zipcode = xl.getCellData(xlfile, tssheet, j, 18);

							sun2.typeofaccount = xl.getCellData(xlfile, tssheet, j, 19);

							res = sun2.SponsorUsersignupNegativeStep2();
							String Srese12 = d
									.findElement(
											By.xpath("html/body/div[3]/form/div[1]/div[2]/div[2]/div[1]/div[2]/label"))
									.getText();
							System.out.println(Srese12);
							d.get(url);
							Sleeper.sleepTightInSeconds(5);
							d.findElement(By.xpath("html/body/div[1]/div/div[1]/div/div/div[3]/ul/li[2]/a")).click();
							break;

						case "SPONSORUSERSIGNUPNEGATIVE14":
							sun1.fname = xl.getCellData(xlfile, tssheet, j, 5);

							sun1.lname = xl.getCellData(xlfile, tssheet, j, 6);

							sun1.email = xl.getCellData(xlfile, tssheet, j, 7);

							sun1.pass = xl.getCellData(xlfile, tssheet, j, 8);

							sun1.cpass = xl.getCellData(xlfile, tssheet, j, 9);

							sun2.DOB = xl.getCellData(xlfile, tssheet, j, 10);

							sun2.SSN = xl.getCellData(xlfile, tssheet, j, 11);

							sun2.maritalstatus = xl.getCellData(xlfile, tssheet, j, 12);

							sun2.phone = xl.getCellData(xlfile, tssheet, j, 13);

							sun2.address1 = xl.getCellData(xlfile, tssheet, j, 14);

							sun2.address2 = xl.getCellData(xlfile, tssheet, j, 15);

							sun2.city = xl.getCellData(xlfile, tssheet, j, 16);

							sun2.state = xl.getCellData(xlfile, tssheet, j, 17);

							sun2.zipcode = xl.getCellData(xlfile, tssheet, j, 18);

							sun2.typeofaccount = xl.getCellData(xlfile, tssheet, j, 19);

							res = sun2.SponsorUsersignupNegativeStep2();
							String Srese14 = d
									.findElement(
											By.xpath("html/body/div[3]/form/div[1]/div[2]/div[2]/div[2]/div[2]/label"))
									.getText();
							System.out.println(Srese14);
							d.get(url);
							Sleeper.sleepTightInSeconds(5);
							d.findElement(By.xpath("html/body/div[1]/div/div[1]/div/div/div[3]/ul/li[2]/a")).click();
							break;

						case "SPONSORUSERSIGNUPNEGATIVE15":
							sun1.fname = xl.getCellData(xlfile, tssheet, j, 5);

							sun1.lname = xl.getCellData(xlfile, tssheet, j, 6);

							sun1.email = xl.getCellData(xlfile, tssheet, j, 7);

							sun1.pass = xl.getCellData(xlfile, tssheet, j, 8);

							sun1.cpass = xl.getCellData(xlfile, tssheet, j, 9);

							sun2.DOB = xl.getCellData(xlfile, tssheet, j, 10);

							sun2.SSN = xl.getCellData(xlfile, tssheet, j, 11);

							sun2.maritalstatus = xl.getCellData(xlfile, tssheet, j, 12);

							sun2.phone = xl.getCellData(xlfile, tssheet, j, 13);

							sun2.address1 = xl.getCellData(xlfile, tssheet, j, 14);

							sun2.address2 = xl.getCellData(xlfile, tssheet, j, 15);

							sun2.city = xl.getCellData(xlfile, tssheet, j, 16);

							sun2.state = xl.getCellData(xlfile, tssheet, j, 17);

							sun2.zipcode = xl.getCellData(xlfile, tssheet, j, 18);

							sun2.typeofaccount = xl.getCellData(xlfile, tssheet, j, 19);

							res = sun2.SponsorUsersignupNegativeStep2();
							String Srese15 = d
									.findElement(
											By.xpath("html/body/div[3]/form/div[1]/div[2]/div[2]/div[2]/div[2]/label"))
									.getText();
							System.out.println(Srese15);
							d.get(url);
							Sleeper.sleepTightInSeconds(5);
							d.findElement(By.xpath("html/body/div[1]/div/div[1]/div/div/div[3]/ul/li[2]/a")).click();
							break;

						case "SPONSORUSERSIGNUPNEGATIVE16":
							sun1.fname = xl.getCellData(xlfile, tssheet, j, 5);

							sun1.lname = xl.getCellData(xlfile, tssheet, j, 6);

							sun1.email = xl.getCellData(xlfile, tssheet, j, 7);

							sun1.pass = xl.getCellData(xlfile, tssheet, j, 8);

							sun1.cpass = xl.getCellData(xlfile, tssheet, j, 9);

							sun2.DOB = xl.getCellData(xlfile, tssheet, j, 10);

							sun2.SSN = xl.getCellData(xlfile, tssheet, j, 11);

							sun2.maritalstatus = xl.getCellData(xlfile, tssheet, j, 12);

							sun2.phone = xl.getCellData(xlfile, tssheet, j, 13);

							sun2.address1 = xl.getCellData(xlfile, tssheet, j, 14);

							sun2.address2 = xl.getCellData(xlfile, tssheet, j, 15);

							sun2.city = xl.getCellData(xlfile, tssheet, j, 16);

							sun2.state = xl.getCellData(xlfile, tssheet, j, 17);

							sun2.zipcode = xl.getCellData(xlfile, tssheet, j, 18);

							sun2.typeofaccount = xl.getCellData(xlfile, tssheet, j, 19);

							res = sun2.SponsorUsersignupNegativeStep2();
							String Srese16 = d
									.findElement(
											By.xpath("html/body/div[3]/form/div[2]/div[2]/div[2]/div[1]/div[1]/label"))
									.getText();
							System.out.println(Srese16);
							d.get(url);
							Sleeper.sleepTightInSeconds(5);
							d.findElement(By.xpath("html/body/div[1]/div/div[1]/div/div/div[3]/ul/li[2]/a")).click();
							break;

						case "SPONSORUSERSIGNUPNEGATIVE17":
							sun1.fname = xl.getCellData(xlfile, tssheet, j, 5);

							sun1.lname = xl.getCellData(xlfile, tssheet, j, 6);

							sun1.email = xl.getCellData(xlfile, tssheet, j, 7);

							sun1.pass = xl.getCellData(xlfile, tssheet, j, 8);

							sun1.cpass = xl.getCellData(xlfile, tssheet, j, 9);

							sun2.DOB = xl.getCellData(xlfile, tssheet, j, 10);

							sun2.SSN = xl.getCellData(xlfile, tssheet, j, 11);

							sun2.maritalstatus = xl.getCellData(xlfile, tssheet, j, 12);

							sun2.phone = xl.getCellData(xlfile, tssheet, j, 13);

							sun2.address1 = xl.getCellData(xlfile, tssheet, j, 14);

							sun2.address2 = xl.getCellData(xlfile, tssheet, j, 15);

							sun2.city = xl.getCellData(xlfile, tssheet, j, 16);

							sun2.state = xl.getCellData(xlfile, tssheet, j, 17);

							sun2.zipcode = xl.getCellData(xlfile, tssheet, j, 18);

							sun2.typeofaccount = xl.getCellData(xlfile, tssheet, j, 19);

							res = sun2.SponsorUsersignupNegativeStep2();
							String Srese17 = d
									.findElement(
											By.xpath("html/body/div[3]/form/div[2]/div[2]/div[2]/div[2]/div[1]/label"))
									.getText();
							System.out.println(Srese17);
							d.get(url);
							Sleeper.sleepTightInSeconds(5);
							d.findElement(By.xpath("html/body/div[1]/div/div[1]/div/div/div[3]/ul/li[2]/a")).click();
							break;

						case "SPONSORUSERSIGNUPNEGATIVE19":
							sun1.fname = xl.getCellData(xlfile, tssheet, j, 5);

							sun1.lname = xl.getCellData(xlfile, tssheet, j, 6);

							sun1.email = xl.getCellData(xlfile, tssheet, j, 7);

							sun1.pass = xl.getCellData(xlfile, tssheet, j, 8);

							sun1.cpass = xl.getCellData(xlfile, tssheet, j, 9);

							sun2.DOB = xl.getCellData(xlfile, tssheet, j, 10);

							sun2.SSN = xl.getCellData(xlfile, tssheet, j, 11);

							sun2.maritalstatus = xl.getCellData(xlfile, tssheet, j, 12);

							sun2.phone = xl.getCellData(xlfile, tssheet, j, 13);

							sun2.address1 = xl.getCellData(xlfile, tssheet, j, 14);

							sun2.address2 = xl.getCellData(xlfile, tssheet, j, 15);

							sun2.city = xl.getCellData(xlfile, tssheet, j, 16);

							sun2.state = xl.getCellData(xlfile, tssheet, j, 17);

							sun2.zipcode = xl.getCellData(xlfile, tssheet, j, 18);

							sun2.typeofaccount = xl.getCellData(xlfile, tssheet, j, 19);

							res = sun2.SponsorUsersignupNegativeStep2();
							String Srese19 = d
									.findElement(
											By.xpath("html/body/div[3]/form/div[2]/div[2]/div[2]/div[2]/div[3]/label"))
									.getText();
							System.out.println(Srese19);
							d.get(url);
							Sleeper.sleepTightInSeconds(5);
							d.findElement(By.xpath("html/body/div[1]/div/div[1]/div/div/div[3]/ul/li[2]/a")).click();
							break;

						case "SPONSORUSERSIGNUPNEGATIVE20":
							sun1.fname = xl.getCellData(xlfile, tssheet, j, 5);

							sun1.lname = xl.getCellData(xlfile, tssheet, j, 6);

							sun1.email = xl.getCellData(xlfile, tssheet, j, 7);

							sun1.pass = xl.getCellData(xlfile, tssheet, j, 8);

							sun1.cpass = xl.getCellData(xlfile, tssheet, j, 9);

							sun2.DOB = xl.getCellData(xlfile, tssheet, j, 10);

							sun2.SSN = xl.getCellData(xlfile, tssheet, j, 11);

							sun2.maritalstatus = xl.getCellData(xlfile, tssheet, j, 12);

							sun2.phone = xl.getCellData(xlfile, tssheet, j, 13);

							sun2.address1 = xl.getCellData(xlfile, tssheet, j, 14);

							sun2.address2 = xl.getCellData(xlfile, tssheet, j, 15);

							sun2.city = xl.getCellData(xlfile, tssheet, j, 16);

							sun2.state = xl.getCellData(xlfile, tssheet, j, 17);

							sun2.zipcode = xl.getCellData(xlfile, tssheet, j, 18);

							sun2.typeofaccount = xl.getCellData(xlfile, tssheet, j, 19);

							res = sun2.SponsorUsersignupNegativeStep2();
							String Srese20 = d
									.findElement(
											By.xpath("html/body/div[3]/form/div[2]/div[2]/div[2]/div[2]/div[3]/label"))
									.getText();
							System.out.println(Srese20);
							d.get(url);
							Sleeper.sleepTightInSeconds(5);
							d.findElement(By.xpath("html/body/div[1]/div/div[1]/div/div/div[3]/ul/li[2]/a")).click();
							break;

						case "SPONSORUSERSIGNUPNEGATIVE21":
							sun1.fname = xl.getCellData(xlfile, tssheet, j, 5);

							sun1.lname = xl.getCellData(xlfile, tssheet, j, 6);

							sun1.email = xl.getCellData(xlfile, tssheet, j, 7);

							sun1.pass = xl.getCellData(xlfile, tssheet, j, 8);

							sun1.cpass = xl.getCellData(xlfile, tssheet, j, 9);

							sun2.DOB = xl.getCellData(xlfile, tssheet, j, 10);

							sun2.SSN = xl.getCellData(xlfile, tssheet, j, 11);

							sun2.maritalstatus = xl.getCellData(xlfile, tssheet, j, 12);

							sun2.phone = xl.getCellData(xlfile, tssheet, j, 13);

							sun2.address1 = xl.getCellData(xlfile, tssheet, j, 14);

							sun2.address2 = xl.getCellData(xlfile, tssheet, j, 15);

							sun2.city = xl.getCellData(xlfile, tssheet, j, 16);

							sun2.state = xl.getCellData(xlfile, tssheet, j, 17);

							sun2.zipcode = xl.getCellData(xlfile, tssheet, j, 18);

							sun2.typeofaccount = xl.getCellData(xlfile, tssheet, j, 19);

							res = sun2.SponsorUsersignupNegativeStep2();
							String Srese21 = d
									.findElement(
											By.xpath("html/body/div[3]/form/div[2]/div[2]/div[2]/div[2]/div[3]/label"))
									.getText();
							System.out.println(Srese21);
							d.get(url);
							Sleeper.sleepTightInSeconds(5);
							d.findElement(By.xpath("html/body/div[1]/div/div[1]/div/div/div[3]/ul/li[2]/a")).click();
							break;

						case "SPONSORUSERSIGNUPNEGATIVE22":
							sun1.fname = xl.getCellData(xlfile, tssheet, j, 5);

							sun1.lname = xl.getCellData(xlfile, tssheet, j, 6);

							sun1.email = xl.getCellData(xlfile, tssheet, j, 7);

							sun1.pass = xl.getCellData(xlfile, tssheet, j, 8);

							sun1.cpass = xl.getCellData(xlfile, tssheet, j, 9);

							sun2.DOB = xl.getCellData(xlfile, tssheet, j, 10);

							sun2.SSN = xl.getCellData(xlfile, tssheet, j, 11);

							sun2.maritalstatus = xl.getCellData(xlfile, tssheet, j, 12);

							sun2.phone = xl.getCellData(xlfile, tssheet, j, 13);

							sun2.address1 = xl.getCellData(xlfile, tssheet, j, 14);

							sun2.address2 = xl.getCellData(xlfile, tssheet, j, 15);

							sun2.city = xl.getCellData(xlfile, tssheet, j, 16);

							sun2.state = xl.getCellData(xlfile, tssheet, j, 17);

							sun2.zipcode = xl.getCellData(xlfile, tssheet, j, 18);

							sun2.typeofaccount = xl.getCellData(xlfile, tssheet, j, 19);

							sun3.cardno = xl.getCellData(xlfile, tssheet, j, 20);

							sun3.cardExpiryDate = xl.getCellData(xlfile, tssheet, j, 21);

							sun3.cardName = xl.getCellData(xlfile, tssheet, j, 22);

							sun3.cardCVCNumber = xl.getCellData(xlfile, tssheet, j, 23);

							res = sun3.SponsorUsersignupNegativeStep3();
							String Srese22 = d
									.findElement(By
											.xpath("html/body/div[3]/form/div[3]/div/div[2]/div[2]/div[1]/div[1]/label"))
									.getText();
							System.out.println(Srese22);
							d.get(url);
							Sleeper.sleepTightInSeconds(5);
							d.findElement(By.xpath("html/body/div[1]/div/div[1]/div/div/div[3]/ul/li[2]/a")).click();

							break;

						case "SPONSORUSERSIGNUPNEGATIVE23":
							sun1.fname = xl.getCellData(xlfile, tssheet, j, 5);

							sun1.lname = xl.getCellData(xlfile, tssheet, j, 6);

							sun1.email = xl.getCellData(xlfile, tssheet, j, 7);

							sun1.pass = xl.getCellData(xlfile, tssheet, j, 8);

							sun1.cpass = xl.getCellData(xlfile, tssheet, j, 9);

							sun2.DOB = xl.getCellData(xlfile, tssheet, j, 10);

							sun2.SSN = xl.getCellData(xlfile, tssheet, j, 11);

							sun2.maritalstatus = xl.getCellData(xlfile, tssheet, j, 12);

							sun2.phone = xl.getCellData(xlfile, tssheet, j, 13);

							sun2.address1 = xl.getCellData(xlfile, tssheet, j, 14);

							sun2.address2 = xl.getCellData(xlfile, tssheet, j, 15);

							sun2.city = xl.getCellData(xlfile, tssheet, j, 16);

							sun2.state = xl.getCellData(xlfile, tssheet, j, 17);

							sun2.zipcode = xl.getCellData(xlfile, tssheet, j, 18);

							sun2.typeofaccount = xl.getCellData(xlfile, tssheet, j, 19);

							sun3.cardno = xl.getCellData(xlfile, tssheet, j, 20);

							sun3.cardExpiryDate = xl.getCellData(xlfile, tssheet, j, 21);

							sun3.cardName = xl.getCellData(xlfile, tssheet, j, 22);

							sun3.cardCVCNumber = xl.getCellData(xlfile, tssheet, j, 23);

							res = sun3.SponsorUsersignupNegativeStep3();
							String Srese23 = d
									.findElement(By
											.xpath("html/body/div[3]/form/div[3]/div/div[2]/div[2]/div[1]/div[1]/label"))
									.getText();
							System.out.println(Srese23);
							d.get(url);
							Sleeper.sleepTightInSeconds(5);
							d.findElement(By.xpath("html/body/div[1]/div/div[1]/div/div/div[3]/ul/li[2]/a")).click();

							break;

						case "SPONSORUSERSIGNUPNEGATIVE24":
							sun1.fname = xl.getCellData(xlfile, tssheet, j, 5);

							sun1.lname = xl.getCellData(xlfile, tssheet, j, 6);

							sun1.email = xl.getCellData(xlfile, tssheet, j, 7);

							sun1.pass = xl.getCellData(xlfile, tssheet, j, 8);

							sun1.cpass = xl.getCellData(xlfile, tssheet, j, 9);

							sun2.DOB = xl.getCellData(xlfile, tssheet, j, 10);

							sun2.SSN = xl.getCellData(xlfile, tssheet, j, 11);

							sun2.maritalstatus = xl.getCellData(xlfile, tssheet, j, 12);

							sun2.phone = xl.getCellData(xlfile, tssheet, j, 13);

							sun2.address1 = xl.getCellData(xlfile, tssheet, j, 14);

							sun2.address2 = xl.getCellData(xlfile, tssheet, j, 15);

							sun2.city = xl.getCellData(xlfile, tssheet, j, 16);

							sun2.state = xl.getCellData(xlfile, tssheet, j, 17);

							sun2.zipcode = xl.getCellData(xlfile, tssheet, j, 18);

							sun2.typeofaccount = xl.getCellData(xlfile, tssheet, j, 19);

							sun3.cardno = xl.getCellData(xlfile, tssheet, j, 20);

							sun3.cardExpiryDate = xl.getCellData(xlfile, tssheet, j, 21);

							sun3.cardName = xl.getCellData(xlfile, tssheet, j, 22);

							sun3.cardCVCNumber = xl.getCellData(xlfile, tssheet, j, 23);

							res = sun3.SponsorUsersignupNegativeStep3();
							String Srese24 = d
									.findElement(By
											.xpath("html/body/div[3]/form/div[3]/div/div[2]/div[2]/div[1]/div[2]/label"))
									.getText();
							System.out.println(Srese24);
							d.get(url);
							Sleeper.sleepTightInSeconds(5);
							d.findElement(By.xpath("html/body/div[1]/div/div[1]/div/div/div[3]/ul/li[2]/a")).click();

							break;

						case "SPONSORUSERSIGNUPNEGATIVE25":
							sun1.fname = xl.getCellData(xlfile, tssheet, j, 5);

							sun1.lname = xl.getCellData(xlfile, tssheet, j, 6);

							sun1.email = xl.getCellData(xlfile, tssheet, j, 7);

							sun1.pass = xl.getCellData(xlfile, tssheet, j, 8);

							sun1.cpass = xl.getCellData(xlfile, tssheet, j, 9);

							sun2.DOB = xl.getCellData(xlfile, tssheet, j, 10);

							sun2.SSN = xl.getCellData(xlfile, tssheet, j, 11);

							sun2.maritalstatus = xl.getCellData(xlfile, tssheet, j, 12);

							sun2.phone = xl.getCellData(xlfile, tssheet, j, 13);

							sun2.address1 = xl.getCellData(xlfile, tssheet, j, 14);

							sun2.address2 = xl.getCellData(xlfile, tssheet, j, 15);

							sun2.city = xl.getCellData(xlfile, tssheet, j, 16);

							sun2.state = xl.getCellData(xlfile, tssheet, j, 17);

							sun2.zipcode = xl.getCellData(xlfile, tssheet, j, 18);

							sun2.typeofaccount = xl.getCellData(xlfile, tssheet, j, 19);

							sun3.cardno = xl.getCellData(xlfile, tssheet, j, 20);

							sun3.cardExpiryDate = xl.getCellData(xlfile, tssheet, j, 21);

							sun3.cardName = xl.getCellData(xlfile, tssheet, j, 22);

							sun3.cardCVCNumber = xl.getCellData(xlfile, tssheet, j, 23);

							res = sun3.SponsorUsersignupNegativeStep3();
							String Srese25 = d
									.findElement(By
											.xpath("html/body/div[3]/form/div[3]/div/div[2]/div[2]/div[1]/div[2]/label"))
									.getText();
							System.out.println(Srese25);
							d.get(url);
							Sleeper.sleepTightInSeconds(5);
							d.findElement(By.xpath("html/body/div[1]/div/div[1]/div/div/div[3]/ul/li[2]/a")).click();

							break;

						case "SPONSORUSERSIGNUPNEGATIVE26":
							sun1.fname = xl.getCellData(xlfile, tssheet, j, 5);

							sun1.lname = xl.getCellData(xlfile, tssheet, j, 6);

							sun1.email = xl.getCellData(xlfile, tssheet, j, 7);

							sun1.pass = xl.getCellData(xlfile, tssheet, j, 8);

							sun1.cpass = xl.getCellData(xlfile, tssheet, j, 9);

							sun2.DOB = xl.getCellData(xlfile, tssheet, j, 10);

							sun2.SSN = xl.getCellData(xlfile, tssheet, j, 11);

							sun2.maritalstatus = xl.getCellData(xlfile, tssheet, j, 12);

							sun2.phone = xl.getCellData(xlfile, tssheet, j, 13);

							sun2.address1 = xl.getCellData(xlfile, tssheet, j, 14);

							sun2.address2 = xl.getCellData(xlfile, tssheet, j, 15);

							sun2.city = xl.getCellData(xlfile, tssheet, j, 16);

							sun2.state = xl.getCellData(xlfile, tssheet, j, 17);

							sun2.zipcode = xl.getCellData(xlfile, tssheet, j, 18);

							sun2.typeofaccount = xl.getCellData(xlfile, tssheet, j, 19);

							sun3.cardno = xl.getCellData(xlfile, tssheet, j, 20);

							sun3.cardExpiryDate = xl.getCellData(xlfile, tssheet, j, 21);

							sun3.cardName = xl.getCellData(xlfile, tssheet, j, 22);

							sun3.cardCVCNumber = xl.getCellData(xlfile, tssheet, j, 23);

							res = sun3.SponsorUsersignupNegativeStep3();
							String Srese26 = d
									.findElement(By
											.xpath("html/body/div[3]/form/div[3]/div/div[2]/div[2]/div[2]/div[1]/label"))
									.getText();
							System.out.println(Srese26);
							d.get(url);
							Sleeper.sleepTightInSeconds(5);
							d.findElement(By.xpath("html/body/div[1]/div/div[1]/div/div/div[3]/ul/li[2]/a")).click();

							break;

						case "SPONSORUSERSIGNUPNEGATIVE27":
							sun1.fname = xl.getCellData(xlfile, tssheet, j, 5);

							sun1.lname = xl.getCellData(xlfile, tssheet, j, 6);

							sun1.email = xl.getCellData(xlfile, tssheet, j, 7);

							sun1.pass = xl.getCellData(xlfile, tssheet, j, 8);

							sun1.cpass = xl.getCellData(xlfile, tssheet, j, 9);

							sun2.DOB = xl.getCellData(xlfile, tssheet, j, 10);

							sun2.SSN = xl.getCellData(xlfile, tssheet, j, 11);

							sun2.maritalstatus = xl.getCellData(xlfile, tssheet, j, 12);

							sun2.phone = xl.getCellData(xlfile, tssheet, j, 13);

							sun2.address1 = xl.getCellData(xlfile, tssheet, j, 14);

							sun2.address2 = xl.getCellData(xlfile, tssheet, j, 15);

							sun2.city = xl.getCellData(xlfile, tssheet, j, 16);

							sun2.state = xl.getCellData(xlfile, tssheet, j, 17);

							sun2.zipcode = xl.getCellData(xlfile, tssheet, j, 18);

							sun2.typeofaccount = xl.getCellData(xlfile, tssheet, j, 19);

							sun3.cardno = xl.getCellData(xlfile, tssheet, j, 20);

							sun3.cardExpiryDate = xl.getCellData(xlfile, tssheet, j, 21);

							sun3.cardName = xl.getCellData(xlfile, tssheet, j, 22);

							sun3.cardCVCNumber = xl.getCellData(xlfile, tssheet, j, 23);

							res = sun3.SponsorUsersignupNegativeStep3();
							String Srese27 = d
									.findElement(By
											.xpath("html/body/div[3]/form/div[3]/div/div[2]/div[2]/div[2]/div[2]/label"))
									.getText();
							System.out.println(Srese27);
							d.get(url);
							Sleeper.sleepTightInSeconds(5);
							d.findElement(By.xpath("html/body/div[1]/div/div[1]/div/div/div[3]/ul/li[2]/a")).click();

							break;

						case "SPONSORUSERSIGNUPNEGATIVE28":
							sun1.fname = xl.getCellData(xlfile, tssheet, j, 5);

							sun1.lname = xl.getCellData(xlfile, tssheet, j, 6);

							sun1.email = xl.getCellData(xlfile, tssheet, j, 7);

							sun1.pass = xl.getCellData(xlfile, tssheet, j, 8);

							sun1.cpass = xl.getCellData(xlfile, tssheet, j, 9);

							sun2.DOB = xl.getCellData(xlfile, tssheet, j, 10);

							sun2.SSN = xl.getCellData(xlfile, tssheet, j, 11);

							sun2.maritalstatus = xl.getCellData(xlfile, tssheet, j, 12);

							sun2.phone = xl.getCellData(xlfile, tssheet, j, 13);

							sun2.address1 = xl.getCellData(xlfile, tssheet, j, 14);

							sun2.address2 = xl.getCellData(xlfile, tssheet, j, 15);

							sun2.city = xl.getCellData(xlfile, tssheet, j, 16);

							sun2.state = xl.getCellData(xlfile, tssheet, j, 17);

							sun2.zipcode = xl.getCellData(xlfile, tssheet, j, 18);

							sun2.typeofaccount = xl.getCellData(xlfile, tssheet, j, 19);

							sun3.cardno = xl.getCellData(xlfile, tssheet, j, 20);

							sun3.cardExpiryDate = xl.getCellData(xlfile, tssheet, j, 21);

							sun3.cardName = xl.getCellData(xlfile, tssheet, j, 22);

							sun3.cardCVCNumber = xl.getCellData(xlfile, tssheet, j, 23);

							res = sun3.SponsorUsersignupNegativeStep3();
							String Srese28 = d
									.findElement(By
											.xpath("html/body/div[3]/form/div[3]/div/div[2]/div[2]/div[2]/div[2]/label"))
									.getText();
							System.out.println(Srese28);
							d.get(url);
							Sleeper.sleepTightInSeconds(5);
							d.findElement(By.xpath("html/body/div[1]/div/div[1]/div/div/div[3]/ul/li[2]/a")).click();

							break;
							
						case "SPONSORCHANGEPASSWORD":
							scp.Cpassword=xl.getCellData(xlfile, tssheet, j, 5);
							scp.Npassword=xl.getCellData(xlfile, tssheet, j, 6);
							scp.newpasswordagain=xl.getCellData(xlfile, tssheet, j, 7);
							res=scp.SponsorChangePassword();
							slog.Logout();
							break;
							
						case "SPONSORCHANGEPASSWORDNEGATIVE":
							scpn.Cpassword=xl.getCellData(xlfile, tssheet, j, 5);
							scpn.Npassword=xl.getCellData(xlfile, tssheet, j, 6);
							scpn.newpasswordagain=xl.getCellData(xlfile, tssheet, j, 7);
							res=scpn.SponsorChangePasswordNegative();
							slog.Logout();
							//System.out.println("SPONSORCHANGEPASSWORDNEGATIVE pass");
							break;
							
						case "SPONSORCHANGEPASSWORDNEGATIVE1":
							scpn.Cpassword=xl.getCellData(xlfile, tssheet, j, 5);
							scpn.Npassword=xl.getCellData(xlfile, tssheet, j, 6);
							scpn.newpasswordagain=xl.getCellData(xlfile, tssheet, j, 7);
							res=scpn.SponsorChangePasswordNegative();
							slog.Logout();
							break;
							
						case "SPONSORCHANGEPASSWORDNEGATIVE2":
							scpn.Cpassword=xl.getCellData(xlfile, tssheet, j, 5);
							scpn.Npassword=xl.getCellData(xlfile, tssheet, j, 6);
							scpn.newpasswordagain=xl.getCellData(xlfile, tssheet, j, 7);
							res=scpn.SponsorChangePasswordNegative();
							slog.Logout();
							break;
							
						case "SPONSORCHANGEPASSWORDNEGATIVE3":
							scpn.Cpassword=xl.getCellData(xlfile, tssheet, j, 5);
							scpn.Npassword=xl.getCellData(xlfile, tssheet, j, 6);
							scpn.newpasswordagain=xl.getCellData(xlfile, tssheet, j, 7);
							res=scpn.SponsorChangePasswordNegative();
							slog.Logout();
							break;
							
						case "SPONSORCHANGEPASSWORDNEGATIVE4":
							scpn.Cpassword=xl.getCellData(xlfile, tssheet, j, 5);
							scpn.Npassword=xl.getCellData(xlfile, tssheet, j, 6);
							scpn.newpasswordagain=xl.getCellData(xlfile, tssheet, j, 7);
							res=scpn.SponsorChangePasswordNegative();
							slog.Logout();
							break;
							
						case "SPONSORCHANGEPASSWORDNEGATIVE5":
							scpn.Cpassword=xl.getCellData(xlfile, tssheet, j, 5);
							scpn.Npassword=xl.getCellData(xlfile, tssheet, j, 6);
							scpn.newpasswordagain=xl.getCellData(xlfile, tssheet, j, 7);
							res=scpn.SponsorChangePasswordNegative();
							slog.Logout();
							break;
							
						case "SPONSORCHANGEPASSWORDNEGATIVE6":
							scpn.Cpassword=xl.getCellData(xlfile, tssheet, j, 5);
							scpn.Npassword=xl.getCellData(xlfile, tssheet, j, 6);
							scpn.newpasswordagain=xl.getCellData(xlfile, tssheet, j, 7);
							res=scpn.SponsorChangePasswordNegative();
							slog.Logout();
							break;
							
						case "SPONSORCHANGEPASSWORDNEGATIVE7":
							scpn.Cpassword=xl.getCellData(xlfile, tssheet, j, 5);
							scpn.Npassword=xl.getCellData(xlfile, tssheet, j, 6);
							scpn.newpasswordagain=xl.getCellData(xlfile, tssheet, j, 7);
							res=scpn.SponsorChangePasswordNegative();
							slog.Logout();
							break;
							
						case "SPONSORINVESTMENTSTATUS":
							sai.name = xl.getCellData(xlfile, tssheet, j, 5);
							sai.Investment = xl.getCellData(xlfile, tssheet, j, 6);
							sai.text = xl.getCellData(xlfile, tssheet, j, 7);
							sis.invstatus=xl.getCellData(xlfile, tssheet, j, 8);
							sis.filestatus=xl.getCellData(xlfile, tssheet, j, 9);
							res=sis.sponsorInvestmentStatus();
							break;
							
						case "SPONSORINVESTMENTFILESTATUS":
							res=sis.sponsorInvestmentDocumentStatus();
							slog.Logout();
							break;
							
							
							

						default:
							break;
						}

						String tsres = null;

						if (res)
						{
							tsres = "Pass";
							xl.setCellData(xlfile, tssheet, j, 3, tsres);
							xl.fillGreenColor(xlfile, tssheet, j, 3);
							tcres = tsres;
							xl.setCellData(xlfile, tcsheet, i, 3, tcres);
							xl.fillGreenColor(xlfile, tcsheet, i, 3);
						} 
						else 
						{
							tsres = "Fail";
							xl.setCellData(xlfile, tssheet, j, 3, tsres);
							xl.fillRedColor(xlfile, tssheet, j, 3);
							tcres = tsres;
							xl.setCellData(xlfile, tcsheet, i, 3, tcres);
							xl.fillRedColor(xlfile, tcsheet, i, 3);
						}
					}
				}
			} 
			else 
			{
				xl.setCellData(xlfile, tcsheet, i, 3, "Not Executed");
				xl.fillOrangeColor(xlfile, tcsheet, i, 3);
				
			}
		}
	}

}
