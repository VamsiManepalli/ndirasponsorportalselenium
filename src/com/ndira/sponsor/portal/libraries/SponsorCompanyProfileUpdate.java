package com.ndira.sponsor.portal.libraries;

import java.io.IOException;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.server.browserlaunchers.Sleeper;
import org.openqa.selenium.support.ui.Select;
import com.ndira.sponsor.portal.utils.NDiraConstants;
import com.ndira.sponsor.portal.utils.NdiraUtils;

public class SponsorCompanyProfileUpdate extends NDiraConstants
{
	public static String Address1,Address2,city,state1,Zipcode,website,Phone1,Phone2;
	public static boolean SponsorCompanyProfileUpdate() throws IOException
	{
		try 
		{		
		Sponsorlogout no=new Sponsorlogout();
	  	no.mLogin();
	  	d.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	  	d.findElement(By.xpath("html/body/div[1]/div/div[2]/div[2]/main/div/div[2]/ui-view/div/div[4]/div/form/div[5]/div/input")).clear();
	  	
	  	d.findElement(By.xpath("html/body/div[1]/div/div[2]/div[2]/main/div/div[2]/ui-view/div/div[4]/div/form/div[6]/div/input")).clear();
	  	
	  	d.findElement(By.xpath("html/body/div[1]/div/div[2]/div[2]/main/div/div[2]/ui-view/div/div[4]/div/form/div[7]/div[1]/input")).clear();
	  	
	  	d.findElement(By.xpath("html/body/div[1]/div/div[2]/div[2]/main/div/div[2]/ui-view/div/div[4]/div/form/div[8]/div/input")).clear();
	  	
	  	d.findElement(By.xpath("html/body/div[1]/div/div[2]/div[2]/main/div/div[2]/ui-view/div/div[4]/div/form/div[9]/div/input")).clear();
	  	
	  	d.findElement(By.xpath("html/body/div[1]/div/div[2]/div[2]/main/div/div[2]/ui-view/div/div[4]/div/form/div[10]/div/input")).clear();
	  	
	  	d.findElement(By.xpath("html/body/div[1]/div/div[2]/div[2]/main/div/div[2]/ui-view/div/div[4]/div/form/div[11]/div/input")).clear();
	  
	  	
	  	d.findElement(By.xpath("html/body/div[1]/div/div[2]/div[2]/main/div/div[2]/ui-view/div/div[4]/div/form/div[5]/div/input")).sendKeys(Address1);
	  	
	  	
	  	d.findElement(By.xpath("html/body/div[1]/div/div[2]/div[2]/main/div/div[2]/ui-view/div/div[4]/div/form/div[6]/div/input")).sendKeys(Address2);
	  	  	
	  	d.findElement(By.xpath("html/body/div[1]/div/div[2]/div[2]/main/div/div[2]/ui-view/div/div[4]/div/form/div[7]/div[1]/input")).sendKeys(city);
	  	
	  	
	  	Select state=new Select(d.findElement(By.xpath("html/body/div[1]/div/div[2]/div[2]/main/div/div[2]/ui-view/div/div[4]/div/form/div[7]/div[2]/select")));
	  	state.selectByVisibleText(state1);
	  	
	    d.findElement(By.xpath("html/body/div[1]/div/div[2]/div[2]/main/div/div[2]/ui-view/div/div[4]/div/form/div[8]/div/input")).sendKeys(Zipcode);
	    
	    d.findElement(By.xpath("html/body/div[1]/div/div[2]/div[2]/main/div/div[2]/ui-view/div/div[4]/div/form/div[9]/div/input")).sendKeys(website);
	  		    
	    d.findElement(By.xpath("html/body/div[1]/div/div[2]/div[2]/main/div/div[2]/ui-view/div/div[4]/div/form/div[10]/div/input")).sendKeys(Phone1);
	  		  	
	  	d.findElement(By.xpath("html/body/div[1]/div/div[2]/div[2]/main/div/div[2]/ui-view/div/div[4]/div/form/div[11]/div/input")).sendKeys(Phone2);
	  	d.findElement(By.xpath("html/body/div[1]/div/div[2]/div[2]/main/div/div[2]/ui-view/div/div[4]/div/form/div[13]/div[1]/div")).click();
	  	Sleeper.sleepTightInSeconds(4);	  	
	  	String windows_component = NdiraUtils.getProperty("file.windows.component");
	  	Runtime.getRuntime().exec(windows_component);
	  	
	  	String expmsg,acmsg;
	  	acmsg=d.findElement(By.xpath("html/body/div[1]/div/div[2]/div[2]/main/div/div[2]/ui-view/div/div[2]/div/p")).getText();
	  	expmsg="Your logos were successfully added";
	  	if (expmsg.equalsIgnoreCase(acmsg)) 
	  	{
	  		return true;	  	
		} 
	  	else 
	  	{
	  		return false;
		}
		
		} 
	catch (Exception e) 
	{
		System.out.println(e);
		return false;
	}
  }
}
