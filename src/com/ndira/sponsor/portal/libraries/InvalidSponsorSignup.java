package com.ndira.sponsor.portal.libraries;

import java.io.IOException;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.server.browserlaunchers.Sleeper;
import org.openqa.selenium.support.ui.Select;


public class InvalidSponsorSignup extends com.ndira.sponsor.portal.utils.NDiraConstants
{
	public static String company,Category,fname,lname,email,number,password,cpass;
	public static boolean InvalidSponsorSignup() throws IOException 
	{
		try 
		{
			d.findElement(By.xpath("html/body/div[1]/div/div/main/div[3]/div/div/div/a")).click();
			d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);		  
			d.findElement(By.id("company")).sendKeys(company);
			Select cat=new Select(d.findElement(By.id("primary-investment-category")));
			cat.selectByVisibleText(Category);
			d.findElement(By.id("name-first")).sendKeys(fname);
			d.findElement(By.id("name-last")).sendKeys(lname);
			d.findElement(By.id("email")).sendKeys(email);
			d.findElement(By.id("phone-1")).sendKeys(number);
			d.findElement(By.id("password")).sendKeys(password);
			d.findElement(By.id("password-again")).sendKeys(cpass);
			d.findElement(By.xpath("html/body/div[1]/div/div/main/div[3]/div/form/div[8]/div/div/button")).click();
			JavascriptExecutor jse = (JavascriptExecutor)d;
		    jse.executeScript("window.scrollBy(0,-250)", "");
		    Sleeper.sleepTightInSeconds(2);
			String expmsg;
			expmsg=d.findElement(By.xpath("html/body/div[1]/div/div/main/div[3]/div/form/div[1]/div/div/div")).getText();
			d.get(url);
			
			if (SponsorMessagesList.getSponsorMessagesList().contains(expmsg)) 
			{
				return true;				
			} 
			else 
			{
				return false;
			}
			
		} 
		catch (Exception e) 
		{
			System.out.println(e);
			return false;
		}
			
	}
}
