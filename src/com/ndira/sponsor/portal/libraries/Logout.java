package com.ndira.sponsor.portal.libraries;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.server.browserlaunchers.Sleeper;

import com.ndira.sponsor.portal.utils.NDiraConstants;
import com.ndira.sponsor.portal.utils.NdiraUtils;
import com.ndira.sponsor.portal.utils.XLUtils;

public class Logout extends NDiraConstants
{
		
	public static void Logout() 
	{
		try 
		{
			Sleeper.sleepTightInSeconds(2);
			d.findElement(By.xpath("html/body/div[1]/div/div[1]/div[1]/ul/li[6]/a/div[2]")).click();			
		} 
		catch (Exception e) 
		{
			System.out.println(e);
		}
    }
	public void mLogin() throws IOException
	  {
		try
		{
		XLUtils xl=new XLUtils();
		String xlfile = NdiraUtils.getProperty("file.xlfile.path");
		String tcsheet="TestCases";
		String tssheet="TestSteps";
		  
		String sname =xl.getCellData(xlfile, tssheet, 2, 5);
		String spass =xl.getCellData(xlfile, tssheet, 2, 6);
		d.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		d.findElement(By.xpath("html/body/div[1]/div/div/main/div[3]/div/form/div[2]/input")).clear();
		
		 d.findElement(By.xpath("html/body/div[1]/div/div/main/div[3]/div/form/div[2]/input")).sendKeys(sname);
		
		 d.findElement(By.xpath("html/body/div[1]/div/div/main/div[3]/div/form/div[3]/input")).clear();
		
		 d.findElement(By.xpath("html/body/div[1]/div/div/main/div[3]/div/form/div[3]/input")).sendKeys(spass);
		
		 d.findElement(By.xpath("html/body/div[1]/div/div/main/div[3]/div/form/div[4]/div[2]/button")).click();
		}
		catch (Exception e) 
		{
			System.out.println(e);
		}
	  }
	
}
