package com.ndira.sponsor.portal.libraries;

import java.io.IOException;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import com.ndira.sponsor.portal.utils.NDiraConstants;


public class NDiraloginFeeSchedules extends NDiraConstants
{
	public static boolean NDiraloginFeeSchedules() throws IOException
	{
		try 
		{
			Logout no=new Logout();
		  	no.mLogin();
		  	d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			d.findElement(By.xpath("html/body/div[1]/div/div[1]/div[1]/ul/li[4]/a/div[2]")).click();
			d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			d.findElement(By.xpath("html/body/div[1]/div/div[1]/div[2]/ul[2]/li[1]/a")).click();
			d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			String expmsg,acmsg;
			expmsg="Fee Schedules Overview";
			acmsg=d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div/div/div/div/div[1]/div/h1")).getText();
			if (expmsg.contains(acmsg))
			{
				return true;			
			} 
			else 
			{
				return false;
			}	
			
		} 
		catch (Exception e) 
		{
			System.out.println(e);
			return false;
		}
	}
}
