package com.ndira.sponsor.portal.libraries;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.server.browserlaunchers.Sleeper;

import com.ndira.sponsor.portal.utils.NDiraConstants;
import com.ndira.sponsor.portal.utils.NdiraUtils;
import com.ndira.sponsor.portal.utils.XLUtils;

public class NdiraLoginUsers extends NDiraConstants
{
	public static boolean NdiraLoginUsers() throws IOException
	{
		try
		{
		XLUtils xl=new XLUtils();
		String xlfile = NdiraUtils.getProperty("file.xlfile.path");
		String tcsheet="TestCases";
		String tssheet="TestSteps";
		Logout no=new Logout();
	  	no.mLogin();
	  	
	  	d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		d.findElement(By.xpath("html/body/div[1]/div/div[1]/div[1]/ul/li[2]/a/div[2]")).click();
		d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		String sponsor=xl.getCellData(xlfile, tssheet, 1, 5);
		d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[1]/input")).sendKeys(sponsor);
		d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[1]/input")).sendKeys(Keys.ENTER);
		d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		d.findElement(By.linkText(sponsor)).click();
		d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[1]/div/a[5]")).click();
			
		d.findElement(By.xpath(".//*[@id='admin-ndira-users']/div/div/div/div[2]/table/tbody/tr[8]/td[2]/input")).click();
		Sleeper.sleepTightInSeconds(2);
		d.findElement(By.xpath(".//*[@id='admin-ndira-users']/div/div/div/div[2]/table/tbody/tr[8]/td[3]/input")).click();
		Sleeper.sleepTightInSeconds(2);
		d.findElement(By.xpath(".//*[@id='admin-ndira-users']/div/div/div/div[2]/table/tbody/tr[8]/td[4]/input")).click();
		Sleeper.sleepTightInSeconds(2);
		String expmsg;
		expmsg=d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[2]/div[1]/div/div")).getText();
		
		if (expmsg.contains("Sponsor successfully updated."))
		{
			return true;		
		} 
		else
		{
			return false;
		}
	} 
	catch (Exception e)
	{
		System.out.println(e);
		return false;
	}
	}
}
