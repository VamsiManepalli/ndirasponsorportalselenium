package com.ndira.sponsor.portal.libraries;

import java.io.IOException;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import com.ndira.sponsor.portal.utils.NDiraConstants;


public class SponsorSignupWebinarVideos extends NDiraConstants
{
	public static boolean SponsorSignupWebinarVideo() throws IOException
	{
		try 
		{
		  Sponsorlogout.mLogin();
		  d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		  d.findElement(By.xpath("html/body/div[1]/div/div[2]/div[1]/ul/li[3]/ul/li[2]/a")).click();
		  d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		  String exptxt;
		  exptxt=d.findElement(By.xpath(".//*[@id='resources-webinar-videos']/div[1]/div/h1")).getText();
		  if (exptxt.equalsIgnoreCase("Webinar Videos"))
		  {
			return true;
		  } 
		  else
		  {
			  return false;
		  }
			
		} 
		catch (Exception e) 
		{
			System.out.println(e);
			return false;
		}
	}
}
