package com.ndira.sponsor.portal.libraries;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.server.browserlaunchers.Sleeper;
import org.openqa.selenium.support.ui.Select;

import com.ndira.sponsor.portal.utils.NDiraConstants;
import com.ndira.sponsor.portal.utils.NdiraUtils;
import com.ndira.sponsor.portal.utils.XLUtils;

public class CompanyOverview extends NDiraConstants
{
	public static String ucrelatipnship,uspicategory,uusrelatiionship,ustype,uexpiry,ctype,amin,amax,sfunctions;
    public static boolean companyOverview() throws IOException
    {

	  try
		{
		  XLUtils xl=new XLUtils();
		  String xlfile = NdiraUtils.getProperty("file.xlfile.path");
		  String windows_component = NdiraUtils.getProperty("file.windows.component");
		  String tcsheet="TestCases";
		  String tssheet="TestSteps";
		  Logout no=new Logout();
		  no.mLogin();
		  d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);	
		  d.findElement(By.xpath("html/body/div[1]/div/div[1]/div[1]/ul/li[2]/a/div[2]")).click();
		  d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		  String sponsor=xl.getCellData(xlfile, tssheet, 1, 5);
		  d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[1]/input")).sendKeys(sponsor);
		  d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[1]/input")).sendKeys(Keys.ENTER);
		  d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		  d.findElement(By.linkText(sponsor)).click();
		  d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);	
		
		  if (ctype.contains("yes")) 
		  {
			  Sleeper.sleepTightInSeconds(2);
			  d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[2]/div[2]/div/div/div[1]/div[1]/div[2]/div[1]/div/label[1]")).click();
		  } 
		  else 
		  {
			  Sleeper.sleepTightInSeconds(2);			  
			 d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[2]/div[2]/div/div/div[1]/div[1]/div[2]/div[1]/div/label[2]")).click();		
		  }
		  
			Select cr=new Select(d.findElement(By.id("client-relationship")));
			cr.selectByVisibleText(ucrelatipnship);
			Select pic=new Select(d.findElement(By.id("primary-investment-category")));
			pic.selectByVisibleText(uspicategory);
			Select sr=new Select(d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[2]/div[2]/div/div/div[1]/div[1]/div[2]/div[4]/select")));
			sr.selectByVisibleText(uusrelatiionship);
			Select st=new Select(d.findElement(By.xpath("//div[2]/div[2]/div/div/div[1]/div[1]/div[2]/div[5]/select")));
			st.selectByVisibleText(ustype);
			Sleeper.sleepTightInSeconds(2);
			d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[2]/div[2]/div/div/div[1]/div[2]/div[2]/div[1]")).click();
			Sleeper.sleepTightInSeconds(5);
			Runtime.getRuntime().exec(windows_component);
			JavascriptExecutor js = (JavascriptExecutor)d;
	        js.executeScript("window.scrollBy(0,-600)", "");
		
	        d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);	
		if (sfunctions.contains("Yes")) 
		{
			
			d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[2]/div[2]/div/div/div[2]/div[1]/div[2]/ul/li[1]/div/label/input")).isSelected();
			
			d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[2]/div[2]/div/div/div[2]/div[1]/div[2]/ul/li[2]/div/label/input")).isSelected();
			
			d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[2]/div[2]/div/div/div[2]/div[1]/div[2]/ul/li[3]/div/label/input")).isSelected();
			
			d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[2]/div[2]/div/div/div[2]/div[1]/div[2]/ul/li[4]/div/label/input")).isSelected();
		} 
		 	
		else 
		{
			d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[2]/div[2]/div/div/div[2]/div[1]/div[2]/ul/li[1]/div/label/input")).isSelected();
			
			d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[2]/div[2]/div/div/div[2]/div[1]/div[2]/ul/li[2]/div/label/input")).isSelected();
			
			d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[2]/div[2]/div/div/div[2]/div[1]/div[2]/ul/li[3]/div/label/input")).isSelected();
			
			d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[2]/div[2]/div/div/div[2]/div[1]/div[2]/ul/li[4]/div/label/input")).isSelected();
			
			d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[2]/div[2]/div/div/div[2]/div[1]/div[2]/ul/li[1]/div/label/input")).click();
			
			d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[2]/div[2]/div/div/div[2]/div[1]/div[2]/ul/li[2]/div/label/input")).click();
			
			d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[2]/div[2]/div/div/div[2]/div[1]/div[2]/ul/li[3]/div/label/input")).click();
			
			d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[2]/div[2]/div/div/div[2]/div[1]/div[2]/ul/li[4]/div/label/input")).click();
		}
		d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		d.findElement(By.id("allocation-expiration")).clear();
		
		d.findElement(By.id("allocation-expiration")).sendKeys(uexpiry);
		

		d.findElement(By.id("allocation-minimum")).clear();
		
		d.findElement(By.id("allocation-minimum")).sendKeys(amin);
		
			
		d.findElement(By.id("allocation-maximum")).clear();
		
		d.findElement(By.id("allocation-maximum")).sendKeys(amax);
		
		d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[2]/div[2]/div/div/div[2]/div[2]/div[2]/ul/li/button")).click();
		d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[2]/div[2]/div/div/div[2]/div[3]/div[2]/ul/li/button")).click();
		String expmsg,actmsg;
		expmsg="Sponsor successfully updated.";
		d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		JavascriptExecutor jse = (JavascriptExecutor)d;
        jse.executeScript("window.scrollBy(0,-250)", "");
		actmsg=d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[2]/div[1]/div/div/div")).getText();
		if (actmsg.contains(expmsg)) 
		{
			return true;
		} 
		else
		{
			return false;
		}

		} 
		catch (Exception e)
		{
			System.out.println(e);
			return false;
		}
		
  }
}
