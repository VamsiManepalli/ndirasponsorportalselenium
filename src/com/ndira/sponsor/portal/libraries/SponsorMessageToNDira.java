package com.ndira.sponsor.portal.libraries;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.server.browserlaunchers.Sleeper;

import com.ndira.sponsor.portal.utils.NDiraConstants;
import com.ndira.sponsor.portal.utils.NdiraUtils;
import com.ndira.sponsor.portal.utils.XLUtils;

public class SponsorMessageToNDira extends NDiraConstants
{
	public static String text;
	public static boolean SponsorMessageToNDira() throws IOException
	{
		try 
		{		
		    Sponsorlogout no=new Sponsorlogout();
		    no.mLogin();
		    d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		    d.findElement(By.xpath("html/body/div[1]/div/div[1]/div/div/div[3]/ul/li[1]/a")).click();	  
			d.findElement(By.xpath("html/body/div[1]/div/div[2]/div[2]/main/div/div[2]/div/div/div[1]/div/form/div[1]/div[3]/textarea")).sendKeys(text);
			d.findElement(By.xpath("html/body/div[1]/div/div[2]/div[2]/main/div/div[2]/div/div/div[1]/div/form/div[2]/div/button")).click();
			String expmsg;
			Sleeper.sleepTightInSeconds(2);
			expmsg=d.findElement(By.xpath("html/body/div[1]/div/div[2]/div[2]/main/div/div[2]/div/div/div[1]/div/p[1]")).getText();
			if (expmsg.equalsIgnoreCase("Message sent successfully.")) 
			{
				return true;
			} 
			else
			{
				return false;
			}
			
			}
			catch (Exception e)
			{
				System.out.println(e);
				return false;
			}
	}
}
