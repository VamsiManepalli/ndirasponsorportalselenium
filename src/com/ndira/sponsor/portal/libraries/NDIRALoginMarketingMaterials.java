package com.ndira.sponsor.portal.libraries;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.server.browserlaunchers.Sleeper;

import com.ndira.sponsor.portal.utils.NDiraConstants;
import com.ndira.sponsor.portal.utils.NdiraUtils;
import com.ndira.sponsor.portal.utils.XLUtils;

public class NDIRALoginMarketingMaterials extends NDiraConstants
{
	public static boolean NDIRALoginMarketingMaterials() throws IOException
	{
		try
		{
			Logout no=new Logout();
		  	no.mLogin();
		  	d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			d.findElement(By.xpath("html/body/div[1]/div/div[1]/div[1]/ul/li[3]/a/div[2]")).click();
			Sleeper.sleepTightInSeconds(2);
			d.findElement(By.xpath("html/body/div[1]/div/div[1]/div[2]/ul[1]/li[3]/a")).click();
			d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			String expurl;
			expurl=d.findElement(By.xpath(".//*[@id='admin-marketing-materials']/div[1]/div/div/h1")).getText();
			if (expurl.equalsIgnoreCase("Marketing Materials"))
			{
				return true;			
			} 
			else 
			{
				return false;
			}	
		} 
		catch (Exception e)
		{
			System.out.println(e);
			return false;
		}
	}
}
