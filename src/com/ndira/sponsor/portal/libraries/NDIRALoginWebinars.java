package com.ndira.sponsor.portal.libraries;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.server.browserlaunchers.Sleeper;

import com.ndira.sponsor.portal.utils.NDiraConstants;
import com.ndira.sponsor.portal.utils.NdiraUtils;
import com.ndira.sponsor.portal.utils.XLUtils;

public class NDIRALoginWebinars extends NDiraConstants
{
	public static boolean NDIRALoginWebinars() throws IOException
	{
		try 
		{
			Logout no=new Logout();
		  	no.mLogin();
		  	d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			d.findElement(By.xpath("html/body/div[1]/div/div[1]/div[1]/ul/li[3]/a/div[2]")).click();
			d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			d.findElement(By.xpath("html/body/div[1]/div/div[1]/div[2]/ul[1]/li[4]/a")).click();
			d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			String exptext,actext;
			exptext=d.findElement(By.xpath(".//*[@id='admin-webinars-list']/div[1]/div/div/h1")).getText();
			actext="Webinars";
			if (exptext.equalsIgnoreCase(actext))
			{
				return true;			
			} 
			else 
			{
				return false;
			}	

		} 
		catch (Exception e) 
		{
			System.out.println(e);
			return false;
		}
	}
}
