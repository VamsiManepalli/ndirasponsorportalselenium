package com.ndira.sponsor.portal.libraries;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.server.browserlaunchers.Sleeper;

import com.ndira.sponsor.portal.utils.NDiraConstants;
import com.ndira.sponsor.portal.utils.NdiraUtils;
import com.ndira.sponsor.portal.utils.XLUtils;

public class ForgetPasswordNegative extends NDiraConstants
{
	public static String email;
	public static boolean ForgetPasswordNegative() 
	{
	try 
	{			 
	  d.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	  d.findElement(By.xpath("html/body/div[1]/div/div/main/div[3]/div/form/div[4]/div[1]/ul/li/a")).click();
	  d.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	  d.findElement(By.xpath("html/body/div[1]/div/div/main/div/div/div/div/form/div[3]/div[1]/input")).sendKeys(email);
	  d.findElement(By.xpath("html/body/div[1]/div/div/main/div/div/div/div/form/div[3]/div[2]/button")).click();
	  Sleeper.sleepTightInSeconds(2);
	  String expmsg;
	  expmsg=d.findElement(By.xpath("html/body/div[1]/div/div/main/div/div/div/div/form/div[1]/div")).getText();
	  if (expmsg.contains("Error: user does not exist.")) 
	  {
		return true;
	  } 
	  else
	  {
		  return false;
	  }
	  
	} 
	catch(Exception e) 
	{
		System.out.println(e);
		return false;		
	}
	
}
}
