package com.ndira.sponsor.portal.libraries;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.server.browserlaunchers.Sleeper;

import com.ndira.sponsor.portal.utils.NDiraConstants;
import com.ndira.sponsor.portal.utils.NdiraUtils;
import com.ndira.sponsor.portal.utils.XLUtils;

public class MarketingMaterials extends NDiraConstants
{
	public static String Title,Desc;
	public static boolean MarketingMaterials() throws IOException
	{
		try 
		{
			XLUtils xl=new XLUtils();
			String xlfile = NdiraUtils.getProperty("file.xlfile.path");
			String tcsheet="TestCases";
			String tssheet="TestSteps";
			
			Logout no=new Logout();
		  	no.mLogin();
		  	d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		
		d.findElement(By.xpath("html/body/div[1]/div/div[1]/div[1]/ul/li[2]/a/div[2]")).click();
		d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		String sponsor=xl.getCellData(xlfile, tssheet, 1, 5);
		d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[1]/input")).sendKeys(sponsor);
		d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[1]/input")).sendKeys(Keys.ENTER);
		d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		d.findElement(By.linkText(sponsor)).click();
		d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[1]/div/a[8]")).click();
		Sleeper.sleepTightInSeconds(2);
		d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[2]/div[2]/div/div/div[1]/div[2]/div[1]")).click();
		Sleeper.sleepTightInSeconds(5);
		String windows_component = NdiraUtils.getProperty("file.windows.component");
		Runtime.getRuntime().exec(windows_component);
		
		d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);	
		Sleeper.sleepTightInSeconds(2);
		d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[2]/div[2]/div/div/div[2]/div[2]/div/form/div[1]/input")).sendKeys(Title);
		d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[2]/div[2]/div/div/div[2]/div[2]/div/form/div[2]/textarea")).sendKeys(Desc);
		d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[2]/div[2]/div/div/div[2]/div[2]/div/form/ul/li[1]/button")).click();
		String expmsg, acmsg;		
		acmsg="updated.";
		Sleeper.sleepTightInSeconds(3);
		expmsg=d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[2]/div[2]/div/div/div[2]/div[2]/div/div/div/div")).getText();
		Sleeper.sleepTightInSeconds(2);
		d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[2]/div[2]/div/div/div[2]/div[2]/div[1]/form/ul/li[2]/button")).click();
		Sleeper.sleepTightInSeconds(2);
		d.findElement(By.xpath("html/body/div[1]/div/div/div[2]/button[2]")).click();
		if (expmsg.contains(acmsg)) 
		{
			return true;
		} 
		else 
		{
			return false;
		}
		
		} 
		catch (Exception e) 
		{
			System.out.println(e);
			return false;
		}
	}

}
