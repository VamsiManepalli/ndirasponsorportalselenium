package com.ndira.sponsor.portal.libraries;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.server.browserlaunchers.Sleeper;

import com.ndira.sponsor.portal.utils.NDiraConstants;
import com.ndira.sponsor.portal.utils.NdiraUtils;
import com.ndira.sponsor.portal.utils.XLUtils;

public class NDiraloginDepositories extends NDiraConstants
{
	public static boolean NDiraloginDepositories() throws IOException
	{
		try 
		{
		    Logout no=new Logout();
		  	no.mLogin();
		  	d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			d.findElement(By.xpath("html/body/div[1]/div/div[1]/div[1]/ul/li[4]/a/div[2]")).click();
			d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			d.findElement(By.xpath("html/body/div[1]/div/div[1]/div[2]/ul[2]/li[2]/a")).click();
			d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			String exptext,actext;
			exptext="Depositories";
			actext=d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[3]/div/h1")).getText();
			if (exptext.contains(actext))
			{
				return true;			
			} 
			else 
			{
				return false;
			}		
		} 
		catch (Exception e) 
		{
			System.out.println(e);
			return false;
		}
	}
}
