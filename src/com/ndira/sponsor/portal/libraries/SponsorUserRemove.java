package com.ndira.sponsor.portal.libraries;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.server.browserlaunchers.Sleeper;

import com.ndira.sponsor.portal.utils.NdiraUtils;
import com.ndira.sponsor.portal.utils.XLUtils;

public class SponsorUserRemove extends NDiraLogin
{
	public static boolean Sponsoruserremove() throws IOException
	{
		try 
		{		
			XLUtils xl=new XLUtils();
			String xlfile = NdiraUtils.getProperty("file.xlfile.path");
			String tcsheet="TestCases";
			String tssheet="TestSteps";
		  	Logout no=new Logout();
		  	no.mLogin();
		  	d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);	
	
		  	d.findElement(By.xpath("html/body/div[1]/div/div[1]/div[1]/ul/li[2]/a/div[2]")).click();
			d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);	
			String sponsor=xl.getCellData(xlfile, tssheet, 1, 5);
			d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[1]/input")).sendKeys(sponsor);
			d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[1]/input")).sendKeys(Keys.ENTER);
			d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);	
			d.findElement(By.linkText(sponsor)).click();
			d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);	
			d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[1]/div/a[6]")).click();
			
			d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);		
			String emailid=d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[2]/div[2]/div/div/div/div/div[2]/div/div[2]/div/table/tbody/tr[2]/td[2]")).getText();
			xl.setCellData(xlfile, tssheet, 12, 5, emailid);
			d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[2]/div[2]/div/div/div/div/div[2]/div/div[2]/div/table/tbody/tr[2]/td[4]/ul/li[2]/button")).click();
			Sleeper.sleepTightInSeconds(2);		
			d.switchTo().alert().sendKeys(emailid);
			d.switchTo().alert().accept();
			Sleeper.sleepTightInSeconds(2);
			String expmsg,acmsg;
			expmsg="removed.";
			acmsg=d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[2]/div[2]/div/div/div/div/div[2]/div/div[2]/div/p")).getText();
			Sleeper.sleepTightInSeconds(2);
			if (acmsg.contains(expmsg))
			{
				return true;	
			} 
			else 
			{
				return false;
			}
			
		} 
		catch (Exception e) 
		{
			System.out.println(e);
			return false;
		}
	}

}
