package com.ndira.sponsor.portal.libraries;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.server.browserlaunchers.Sleeper;

import com.ndira.sponsor.portal.utils.NDiraConstants;
import com.ndira.sponsor.portal.utils.NdiraUtils;
import com.ndira.sponsor.portal.utils.XLUtils;

public class NDIRALoginDidYouKnow extends NDiraConstants
{
	public static String subject;
	public static boolean NDiraDidYouKnow() throws IOException 
	{
		try 
		{
			Logout no=new Logout();
			no.mLogin();
			d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			d.findElement(By.xpath("html/body/div[1]/div/div[1]/div[1]/ul/li[3]/a/div[2]")).click();
			Sleeper.sleepTightInSeconds(2);
			d.findElement(By.xpath("html/body/div[1]/div/div[1]/div[2]/ul[1]/li[1]/a")).click();
			d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[1]/div/div/button")).click();
			d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[8]/div/div/form/div/input")).sendKeys(subject);
			d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[8]/div/div/form/ul/li[1]/button")).click();
			Sleeper.sleepTightInSeconds(2);
			String expmsg;
			expmsg=d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div/div")).getText();
			if (expmsg.contains("Did You Know saved.")) 
			{
				return true;
			} 
			else 
			{
				return false;
			}
			
		} 
		catch (Exception e)
		{
			System.out.println(e);
			return false;
		}
		
	}
}
