package com.ndira.sponsor.portal.libraries;

import java.io.IOException;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import com.ndira.sponsor.portal.utils.NDiraConstants;
public class SponsorDepositories extends NDiraConstants
{
	public static boolean SponsorDepositories() throws IOException
	{
		try 
		{
		  Sponsorlogout no=new Sponsorlogout();
		  no.mLogin();
		  d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		  d.findElement(By.xpath("html/body/div[1]/div/div[2]/div[1]/ul/li[6]/ul/li[3]/a")).click();
		  d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);	
		  d.findElement(By.xpath("html/body/div[1]/div/div[2]/div[2]/main/div/div[2]/ui-view/div/div[2]/div/div/div[2]/div[1]/div[1]/label/input")).click();
		  d.findElement(By.xpath(".//*[@id='sponsor-depositories']/div[2]/div[1]/div[22]/button")).click();
		  String expmsg;
		  expmsg=d.findElement(By.xpath("html/body/div[1]/div/div[2]/div[2]/main/div/div[2]/ui-view/div/div[2]/div/div/div[1]/div/div")).getText();
		  if (expmsg.contains("Depositories updated.")) 
		  {
			return true;
		  } 
		  else
		  {
			  return false;
		  }
			
		}
		catch (Exception e) 
		{
			System.out.println(e);
			return false;
		}
	}
}
