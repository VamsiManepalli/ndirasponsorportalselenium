package com.ndira.sponsor.portal.libraries;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.server.browserlaunchers.Sleeper;
import org.openqa.selenium.support.ui.Select;

import com.ndira.sponsor.portal.utils.NDiraConstants;
import com.ndira.sponsor.portal.utils.NdiraUtils;
import com.ndira.sponsor.portal.utils.XLUtils;

public class Sponsersignup extends NDiraConstants
{
	public static String company,Category,fname,lname,email,number,password,cpass;
	public static boolean sponserSignup() throws IOException
	{
		try
		{		
			XLUtils xl=new XLUtils();
			String xlfile = NdiraUtils.getProperty("file.xlfile.path");
			String tcsheet="TestCases";
			String tssheet="TestSteps";
			Sleeper.sleepTightInSeconds(2);
			d.findElement(By.xpath("html/body/div[1]/div/div/main/div[3]/div/div/div/a")).click();
		  	Sleeper.sleepTightInSeconds(3);
			d.findElement(By.id("company")).sendKeys(company);
			Select cat=new Select(d.findElement(By.id("primary-investment-category")));
			cat.selectByVisibleText(Category);
			Sleeper.sleepTightInSeconds(2);
			d.findElement(By.id("name-first")).sendKeys(fname);
			Sleeper.sleepTightInSeconds(2);
			d.findElement(By.id("name-last")).sendKeys(lname);
			Sleeper.sleepTightInSeconds(2);
			d.findElement(By.id("email")).sendKeys(email);
			Sleeper.sleepTightInSeconds(2);
			d.findElement(By.id("phone-1")).sendKeys(number);
			Sleeper.sleepTightInSeconds(2);
			d.findElement(By.id("password")).sendKeys(password);
			Sleeper.sleepTightInSeconds(2);
			d.findElement(By.id("password-again")).sendKeys(cpass);
			Sleeper.sleepTightInSeconds(2);
			d.findElement(By.xpath("html/body/div[1]/div/div/main/div[3]/div/form/div[8]/div/div/button")).click();
			Sleeper.sleepTightInSeconds(10);
			d.findElement(By.xpath("html/body/div[1]/div/div[1]/div/div/div[3]/ul/li[2]/a")).click();
			Sleeper.sleepTightInSeconds(3);
			String expurl,acurl;
			Sleeper.sleepTightInSeconds(2);
			expurl=url;
			acurl=d.getCurrentUrl();
			if (expurl.contains(expurl)) 
			{
			   return true;	
			} 
			else
			{
				return false;
			}
		} 
		catch (Exception e)
		{
			System.out.println(e);
			d.get(url);
			return false;
		}
			
	}

}
