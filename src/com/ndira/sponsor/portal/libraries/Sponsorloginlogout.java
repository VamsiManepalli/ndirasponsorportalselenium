package com.ndira.sponsor.portal.libraries;

import java.io.IOException;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import com.ndira.sponsor.portal.utils.NDiraConstants;
import com.ndira.sponsor.portal.utils.NdiraUtils;
import com.ndira.sponsor.portal.utils.XLUtils;

public class Sponsorloginlogout extends NDiraConstants
{
	public static String email,password;
	public boolean Login(String email,String password) throws IOException
	{
		try
		{
		  XLUtils xl=new XLUtils();
		  String xlfile = NdiraUtils.getProperty("file.xlfile.path");
	      String tcsheet="TestCases";
		  String tssheet="TestSteps";
		  d.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		  d.findElement(By.xpath("html/body/div[1]/div/div/main/div[3]/div/form/div[2]/input")).sendKeys(email);
		  d.findElement(By.xpath("html/body/div[1]/div/div/main/div[3]/div/form/div[3]/input")).sendKeys(password);
		  d.findElement(By.xpath("html/body/div[1]/div/div/main/div[3]/div/form/div[4]/div[2]/button")).click();
		  String expmsg,actmsg;
		  expmsg=d.findElement(By.xpath("html/body/div[1]/div/div[1]/div/div/div[2]")).getText();
		  actmsg=xl.getCellData(xlfile, tssheet, 1, 5);
		   if (expmsg.equalsIgnoreCase(actmsg))
		   {
			   d.findElement(By.xpath("html/body/div[1]/div/div[1]/div/div/div[3]/ul/li[2]/a")).click();
			   return true;
		   } 
		   else
		   {
			   return false;
		   }
		
		}
		catch (Exception e)
		{
			System.out.println(e);
			return false;
		}
	  
	   }
  

}
