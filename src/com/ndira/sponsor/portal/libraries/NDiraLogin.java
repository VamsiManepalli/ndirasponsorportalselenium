package com.ndira.sponsor.portal.libraries;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.server.browserlaunchers.Sleeper;

import com.ndira.sponsor.portal.utils.NDiraConstants;
import com.ndira.sponsor.portal.utils.NdiraUtils;
import com.ndira.sponsor.portal.utils.XLUtils;

public class NDiraLogin extends NDiraConstants 
{
	
	public static String suname, spwd;
	public boolean Login() throws IOException 
	{
		try
		{
			XLUtils xl = new XLUtils();
			String xlfile = NdiraUtils.getProperty("file.xlfile.path");
			String tcsheet = "TestCases";
			String tssheet = "TestSteps";
			d.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			d.manage().window().maximize();
			String expurl, acturl;
			expurl = "/admin-new/home";
			d.findElement(By.xpath("html/body/div[1]/div/div/main/div[3]/div/form/div[2]/input")).sendKeys(suname);
			Sleeper.sleepTightInSeconds(5);
			d.findElement(By.xpath("html/body/div[1]/div/div/main/div[3]/div/form/div[3]/input")).sendKeys(spwd);
			d.findElement(By.xpath("html/body/div[1]/div/div/main/div[3]/div/form/div[4]/div[2]/button")).click();
			d.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			Sleeper.sleepTightInSeconds(2);
			acturl = d.getCurrentUrl();
			if (acturl.contains(expurl))
			{
				d.findElement(By.xpath("html/body/div[1]/div/div[1]/div[1]/ul/li[6]/a")).click();
				return true;

			} 
			else
			{
				return false;
			}
		} 
		catch (Exception e)
		{
			System.out.println(e);
			return false;
		}

	}
}
