package com.ndira.sponsor.portal.libraries;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.server.browserlaunchers.Sleeper;

import com.ndira.sponsor.portal.utils.NDiraConstants;
import com.ndira.sponsor.portal.utils.NdiraUtils;
import com.ndira.sponsor.portal.utils.XLUtils;

public class SponsorCurrentInvestments extends NDiraConstants
{
	public static boolean SponsorCurrentInvestigations() throws IOException
	{
		try 
		{
	  	Sponsorlogout no=new Sponsorlogout();
	  	no.mLogin();
	  	d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
	  	d.findElement(By.xpath("html/body/div[1]/div/div[2]/div[1]/ul/li[4]/ul/li[2]/a")).click();
	  	d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
	  	d.findElement(By.xpath("html/body/div[1]/div/div[2]/div[2]/main/div/div/div[1]/ui-view/div/div/div/div/div/div/form/ul/li[1]/button")).click();
	  	d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
	  	d.findElement(By.xpath("html/body/div[1]/div/div/div[2]/button[2]")).click();
	  	Sleeper.sleepTightInSeconds(2);
	  	String expres;
	  	expres=d.findElement(By.xpath("html/body/div[1]/div/div[2]/div[2]/main/div/div/div[1]/ui-view/div/div/div/p")).getText();
	  	if (expres.contains("No investments found.")) 
	  	{
	  		return true;
		} 
	  	else 
	  	{
	  		return false;
		}
		
		} 
		catch (Exception e) 
		{
			System.out.println(e);
			return false;
		}
	  	
	}
}
