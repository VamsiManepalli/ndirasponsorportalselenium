package com.ndira.sponsor.portal.libraries;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.server.browserlaunchers.Sleeper;

import com.ndira.sponsor.portal.utils.NDiraConstants;

public class NdiraInvalidLogin extends NDiraConstants
{
	public static String isuname,ispwd;

	  public boolean ndiraInvalidLogin(String uname,String pwd)
	  {
		  try 
		  {
			  d.manage().window().maximize();
			  String expurl,acturl;
			  expurl="/admin-new/home";
			  d.findElement(By.xpath("html/body/div[1]/div/div/main/div[3]/div/form/div[2]/input")).sendKeys(uname);
			  Sleeper.sleepTightInSeconds(5);
			  d.findElement(By.xpath("html/body/div[1]/div/div/main/div[3]/div/form/div[3]/input")).sendKeys(pwd);
			  d.findElement(By.xpath("html/body/div[1]/div/div/main/div[3]/div/form/div[4]/div[2]/button")).click();
			  Sleeper.sleepTightInSeconds(10);
			  acturl=d.getCurrentUrl();
			  if (acturl.contains(expurl))
			  {
				  return true;
			  } 
			  else
			  {
				  return false;
			  }
		  }
		  catch (Exception e)
		  {
			System.out.println(e);
			return false;
		  }
	  }

}
