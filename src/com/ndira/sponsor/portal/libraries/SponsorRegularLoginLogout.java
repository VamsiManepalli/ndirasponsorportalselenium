package com.ndira.sponsor.portal.libraries;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.server.browserlaunchers.Sleeper;

import com.ndira.sponsor.portal.utils.NDiraConstants;

public class SponsorRegularLoginLogout extends NDiraConstants
{
	public static String uname,pass;
	public static boolean SponsorRegularLoginLogout()
	{
		try 
		{
		  String expurl,acturl;
		  acturl=d.getCurrentUrl();
		  d.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		  Sleeper.sleepTightInSeconds(2);
		  d.findElement(By.xpath("html/body/div[1]/div/div/main/div[3]/div/form/div[2]/input")).sendKeys(uname);
		  d.findElement(By.xpath("html/body/div[1]/div/div/main/div[3]/div/form/div[3]/input")).sendKeys(pass);
		  d.findElement(By.xpath("html/body/div[1]/div/div/main/div[3]/div/form/div[4]/div[2]/button")).click();
		  d.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		  d.findElement(By.xpath("html/body/div[1]/div/div[1]/div/div/div[3]/ul/li[2]/a")).click();
		   expurl=d.getCurrentUrl();
		   
		   if (acturl.contains(expurl))
		   {
			   return true;
		   } 
		   else
		   {
			   return false;
		   }	

		} 
		catch (Exception e)
		{
			System.out.println(e);
			return false;
		}
	}
}
