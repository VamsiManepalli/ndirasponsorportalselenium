package com.ndira.sponsor.portal.libraries;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.server.browserlaunchers.Sleeper;

import com.ndira.sponsor.portal.utils.NDiraConstants;
import com.ndira.sponsor.portal.utils.NdiraUtils;
import com.ndira.sponsor.portal.utils.XLUtils;

public class SponsorChangePassword extends NDiraConstants
{
	public static String Cpassword,Npassword,newpasswordagain;
	public static boolean SponsorChangePassword() throws IOException 
	{
		try
		{
			XLUtils xl=new XLUtils();
			String xlfile = NdiraUtils.getProperty("file.xlfile.path");
			String tcsheet="TestCases";
			String tssheet="TestSteps";
			Sponsorlogout no=new Sponsorlogout();
			no.mLogin();
			d.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			d.findElement(By.xpath("html/body/div[1]/div/div[2]/div[1]/ul/li[6]/ul/li[4]/a")).click();
			
			d.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);			
			d.findElement(By.xpath("html/body/div[1]/div/div[2]/div[2]/main/div/div[2]/ui-view/div[3]/div/form/div[3]/div/input")).sendKeys(Cpassword);
			d.findElement(By.xpath("html/body/div[1]/div/div[2]/div[2]/main/div/div[2]/ui-view/div[3]/div/form/div[4]/div/input")).sendKeys(Npassword);
			d.findElement(By.xpath("html/body/div[1]/div/div[2]/div[2]/main/div/div[2]/ui-view/div[3]/div/form/div[5]/div/input")).sendKeys(newpasswordagain);
						
			xl.setCellData(xlfile, tssheet, 1, 11, Npassword);
			xl.setCellData(xlfile, tssheet, 1, 12, Npassword);
			
			d.findElement(By.xpath("html/body/div[1]/div/div[2]/div[2]/main/div/div[2]/ui-view/div[3]/div/form/div[1]/ul/li/btn")).click();
			Sleeper.sleepTightInSeconds(2);
			
			String acmsg;
			acmsg=d.findElement(By.xpath("html/body/div[1]/div/div[2]/div[2]/main/div/div[2]/ui-view/div[3]/div/form/div[2]/p")).getText();
			
			if (acmsg.equalsIgnoreCase("Password successfully updated."))
			{
				return true;
			}
			else 
			{
				return false;
			}
	
		}
		catch (Exception e)
		{
			System.out.println(e);
			return false;
		}
	}
}
