package com.ndira.sponsor.portal.libraries;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.server.browserlaunchers.Sleeper;
import org.openqa.selenium.support.ui.Select;

import com.ndira.sponsor.portal.utils.NDiraConstants;
import com.ndira.sponsor.portal.utils.NdiraUtils;
import com.ndira.sponsor.portal.utils.XLUtils;

public class CompanyEdit extends NDiraConstants
{
	public static String saddress1,saddress2,scity,szipcode,state,website,p1,p2;
	public static boolean companyEdit(String address1,String address2,String city,String zipcode,String state,String website,String p1,String p2) throws IOException
	{
		try
		{
			XLUtils xl=new XLUtils();
			String xlfile = NdiraUtils.getProperty("file.xlfile.path");
			String tcsheet="TestCases";
			String tssheet="TestSteps";
			Logout no=new Logout();
			no.mLogin();
			
			d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);		
			d.findElement(By.xpath("html/body/div[1]/div/div[1]/div[1]/ul/li[2]/a/div[2]")).click();
			d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			String sponsor=xl.getCellData(xlfile, tssheet, 1, 5);
			d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[1]/input")).sendKeys(sponsor);
			d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[1]/input")).sendKeys(Keys.ENTER);
			d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			d.findElement(By.linkText(sponsor)).click();
			
			d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			d.findElement(By.xpath("//div/div[2]/div[2]/div[2]/div/div/div[1]/div[1]/div[1]/button")).click();
			
			d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			
			d.findElement(By.xpath("html/body/div[1]/div/div/div[2]/form/div[3]/div/input")).clear();
			
			d.findElement(By.xpath("html/body/div[1]/div/div/div[2]/form/div[4]/div/input")).clear();
			
			d.findElement(By.xpath("html/body/div[1]/div/div/div[2]/form/div[5]/div[1]/input")).clear();
			
			d.findElement(By.xpath("html/body/div[1]/div/div/div[2]/form/div[6]/div[1]/input")).clear();
			
			d.findElement(By.xpath("html/body/div[1]/div/div/div[2]/form/div[7]/div/input")).clear();
			
			d.findElement(By.xpath("html/body/div[1]/div/div/div[2]/form/div[8]/div/input")).clear();
			
			d.findElement(By.xpath("html/body/div[1]/div/div/div[2]/form/div[9]/div/input")).clear();
			
			d.findElement(By.xpath("html/body/div[1]/div/div/div[2]/form/div[3]/div/input")).sendKeys(address1);
			
			d.findElement(By.xpath("html/body/div[1]/div/div/div[2]/form/div[4]/div/input")).sendKeys(address2);
			
			d.findElement(By.xpath("html/body/div[1]/div/div/div[2]/form/div[5]/div[1]/input")).sendKeys(city);
			
			
			Select sta=new Select(d.findElement(By.xpath("html/body/div[1]/div/div/div[2]/form/div[5]/div[2]/select")));
			sta.selectByVisibleText(state);
			d.findElement(By.xpath("html/body/div[1]/div/div/div[2]/form/div[6]/div[1]/input")).sendKeys(zipcode);
			
			
			d.findElement(By.xpath("html/body/div[1]/div/div/div[2]/form/div[7]/div/input")).sendKeys(website);
			
			
			d.findElement(By.xpath("html/body/div[1]/div/div/div[2]/form/div[8]/div/input")).sendKeys(p1);
			
			
			d.findElement(By.xpath("html/body/div[1]/div/div/div[2]/form/div[9]/div/input")).sendKeys(p2);
			
			d.findElement(By.xpath("html/body/div[1]/div/div/div[3]/button[2]")).click();
			d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			String expmsg=d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/main/div/div[2]/div[2]/div[1]/div/div")).getText();
			String actmsg="Sponsor details updated";
			if (expmsg.contains(actmsg)) 
			{
				return true;	
			} 
			else 
			{
				return false;
			}
			
		} 
		catch (Exception e)
		{
			d.findElement(By.xpath("html/body/div[1]/div/div/div[3]/button[1]")).click();
			System.out.println(e);
			return false;
		}
	}
}
