package com.ndira.sponsor.portal.libraries;

import java.io.IOException;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import com.ndira.sponsor.portal.utils.NDiraConstants;


public class SponsorSignupFAQs extends NDiraConstants
{
public static boolean SponsorSignupFAQs() throws IOException 
{
	try 
	{
		
		Sponsorlogout no=new Sponsorlogout();
	  	no.mLogin();
	  	d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
	  	d.findElement(By.xpath("html/body/div[1]/div/div[2]/div[1]/ul/li[3]/ul/li[3]/a")).click();
	  	d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
	  	
	  	String exptext;
	  	exptext=d.findElement(By.xpath(".//*[@id='resources-faqs']/div[1]/div/h1")).getText();
	  	
	  	if (exptext.equalsIgnoreCase("FAQs")) 
	  	{
	  		return true;
	  	} 
	  	else
	  	{
		  return false;
	  	}
	} 
	catch (Exception e) 
	{
		System.out.println(e);
		return false;
	}
 }
}
